//--------------------------------------------------------------------------------------------------
// <auto-generated>
//
//     This code was generated by code generator tool.
//
//     To customize the code use your own partial class. For more info about how to use and customize
//     the generated code see the documentation at http://docs.kentico.com.
//
// </auto-generated>
//--------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;

using CMS;
using CMS.Base;
using CMS.Helpers;
using CMS.DataEngine;
using CMS.DocumentEngine.Types.AM;
using CMS.DocumentEngine;

[assembly: RegisterDocumentType(Cooperation.CLASS_NAME, typeof(Cooperation))]

namespace CMS.DocumentEngine.Types.AM
{
	/// <summary>
	/// Represents a content item of type Cooperation.
	/// </summary>
	public partial class Cooperation : TreeNode
	{
		#region "Constants and variables"

		/// <summary>
		/// The name of the data class.
		/// </summary>
		public const string CLASS_NAME = "AM.Cooperation";


		/// <summary>
		/// The instance of the class that provides extended API for working with Cooperation fields.
		/// </summary>
		private readonly CooperationFields mFields;

		#endregion


		#region "Properties"

		/// <summary>
		/// CooperationID.
		/// </summary>
		[DatabaseIDField]
		public int CooperationID
		{
			get
			{
				return ValidationHelper.GetInteger(GetValue("CooperationID"), 0);
			}
			set
			{
				SetValue("CooperationID", value);
			}
		}


		/// <summary>
		/// Cooperation Title.
		/// </summary>
		[DatabaseField]
		public string CooperationTitle
		{
			get
			{
				return ValidationHelper.GetString(GetValue("CooperationTitle"), @"");
			}
			set
			{
				SetValue("CooperationTitle", value);
			}
		}


		/// <summary>
		/// Cooperation Hero Image.
		/// </summary>
		[DatabaseField]
		public string CooperationHeroImage
		{
			get
			{
				return ValidationHelper.GetString(GetValue("CooperationHeroImage"), @"");
			}
			set
			{
				SetValue("CooperationHeroImage", value);
			}
		}


		/// <summary>
		/// Cooperation Text.
		/// </summary>
		[DatabaseField]
		public string CooperationText
		{
			get
			{
				return ValidationHelper.GetString(GetValue("CooperationText"), @"");
			}
			set
			{
				SetValue("CooperationText", value);
			}
		}


		/// <summary>
		/// Gets an object that provides extended API for working with Cooperation fields.
		/// </summary>
		[RegisterProperty]
		public CooperationFields Fields
		{
			get
			{
				return mFields;
			}
		}


		/// <summary>
		/// Provides extended API for working with Cooperation fields.
		/// </summary>
		[RegisterAllProperties]
		public partial class CooperationFields : AbstractHierarchicalObject<CooperationFields>
		{
			/// <summary>
			/// The content item of type Cooperation that is a target of the extended API.
			/// </summary>
			private readonly Cooperation mInstance;


			/// <summary>
			/// Initializes a new instance of the <see cref="CooperationFields" /> class with the specified content item of type Cooperation.
			/// </summary>
			/// <param name="instance">The content item of type Cooperation that is a target of the extended API.</param>
			public CooperationFields(Cooperation instance)
			{
				mInstance = instance;
			}


			/// <summary>
			/// CooperationID.
			/// </summary>
			public int ID
			{
				get
				{
					return mInstance.CooperationID;
				}
				set
				{
					mInstance.CooperationID = value;
				}
			}


			/// <summary>
			/// Cooperation Title.
			/// </summary>
			public string Title
			{
				get
				{
					return mInstance.CooperationTitle;
				}
				set
				{
					mInstance.CooperationTitle = value;
				}
			}


			/// <summary>
			/// Cooperation Hero Image.
			/// </summary>
			public string HeroImage
			{
				get
				{
					return mInstance.CooperationHeroImage;
				}
				set
				{
					mInstance.CooperationHeroImage = value;
				}
			}


			/// <summary>
			/// Cooperation Text.
			/// </summary>
			public string Text
			{
				get
				{
					return mInstance.CooperationText;
				}
				set
				{
					mInstance.CooperationText = value;
				}
			}
		}

		#endregion


		#region "Constructors"

		/// <summary>
		/// Initializes a new instance of the <see cref="Cooperation" /> class.
		/// </summary>
		public Cooperation() : base(CLASS_NAME)
		{
			mFields = new CooperationFields(this);
		}

		#endregion
	}
}