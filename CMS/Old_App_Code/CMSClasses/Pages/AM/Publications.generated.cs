//--------------------------------------------------------------------------------------------------
// <auto-generated>
//
//     This code was generated by code generator tool.
//
//     To customize the code use your own partial class. For more info about how to use and customize
//     the generated code see the documentation at http://docs.kentico.com.
//
// </auto-generated>
//--------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;

using CMS;
using CMS.Base;
using CMS.Helpers;
using CMS.DataEngine;
using CMS.DocumentEngine.Types.AM;
using CMS.DocumentEngine;

[assembly: RegisterDocumentType(Publications.CLASS_NAME, typeof(Publications))]

namespace CMS.DocumentEngine.Types.AM
{
	/// <summary>
	/// Represents a content item of type Publications.
	/// </summary>
	public partial class Publications : TreeNode
	{
		#region "Constants and variables"

		/// <summary>
		/// The name of the data class.
		/// </summary>
		public const string CLASS_NAME = "AM.Publications";


		/// <summary>
		/// The instance of the class that provides extended API for working with Publications fields.
		/// </summary>
		private readonly PublicationsFields mFields;

		#endregion


		#region "Properties"

		/// <summary>
		/// PublicationsID.
		/// </summary>
		[DatabaseIDField]
		public int PublicationsID
		{
			get
			{
				return ValidationHelper.GetInteger(GetValue("PublicationsID"), 0);
			}
			set
			{
				SetValue("PublicationsID", value);
			}
		}


		/// <summary>
		/// Publications Title.
		/// </summary>
		[DatabaseField]
		public string PublicationsTitle
		{
			get
			{
				return ValidationHelper.GetString(GetValue("PublicationsTitle"), @"");
			}
			set
			{
				SetValue("PublicationsTitle", value);
			}
		}


		/// <summary>
		/// Publications Hero Image.
		/// </summary>
		[DatabaseField]
		public string PublicationsHeroImage
		{
			get
			{
				return ValidationHelper.GetString(GetValue("PublicationsHeroImage"), @"");
			}
			set
			{
				SetValue("PublicationsHeroImage", value);
			}
		}


		/// <summary>
		/// Gets an object that provides extended API for working with Publications fields.
		/// </summary>
		[RegisterProperty]
		public PublicationsFields Fields
		{
			get
			{
				return mFields;
			}
		}


		/// <summary>
		/// Provides extended API for working with Publications fields.
		/// </summary>
		[RegisterAllProperties]
		public partial class PublicationsFields : AbstractHierarchicalObject<PublicationsFields>
		{
			/// <summary>
			/// The content item of type Publications that is a target of the extended API.
			/// </summary>
			private readonly Publications mInstance;


			/// <summary>
			/// Initializes a new instance of the <see cref="PublicationsFields" /> class with the specified content item of type Publications.
			/// </summary>
			/// <param name="instance">The content item of type Publications that is a target of the extended API.</param>
			public PublicationsFields(Publications instance)
			{
				mInstance = instance;
			}


			/// <summary>
			/// PublicationsID.
			/// </summary>
			public int ID
			{
				get
				{
					return mInstance.PublicationsID;
				}
				set
				{
					mInstance.PublicationsID = value;
				}
			}


			/// <summary>
			/// Publications Title.
			/// </summary>
			public string Title
			{
				get
				{
					return mInstance.PublicationsTitle;
				}
				set
				{
					mInstance.PublicationsTitle = value;
				}
			}


			/// <summary>
			/// Publications Hero Image.
			/// </summary>
			public string HeroImage
			{
				get
				{
					return mInstance.PublicationsHeroImage;
				}
				set
				{
					mInstance.PublicationsHeroImage = value;
				}
			}
		}

		#endregion


		#region "Constructors"

		/// <summary>
		/// Initializes a new instance of the <see cref="Publications" /> class.
		/// </summary>
		public Publications() : base(CLASS_NAME)
		{
			mFields = new PublicationsFields(this);
		}

		#endregion
	}
}