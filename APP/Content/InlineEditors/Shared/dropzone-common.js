﻿window.project = window.project || {};

(function (dropzoneCommon) {
    /** List of files accepted by the Dropzone object. */
    dropzoneCommon.acceptedFiles = ".bmp, .gif, .ico, .png, .wmf, .jpg, .jpeg, .tiff, .tif";

    /**
     * Handles error codes
     * @param {number} statusCode HTTP status code
     * @param {object} localizationService Kentico localization service
     */
    dropzoneCommon.processErrors = function (statusCode, localizationService) {
        var errorFlag = "error";

        if (statusCode >= 500) {
            window.project.showMessage(localizationService.getString("FormComponent.DropzoneCommon.UploadFailed"), errorFlag);
        } else if (statusCode === 422) {
            window.project.showMessage(localizationService.getString("FormComponent.DropzoneCommon.UploadUnprocessable"), errorFlag);
        } else {
            window.project.showMessage(localizationService.getString("FormComponent.DropzoneCommon.UploadUnknownError"), errorFlag);
        }
    };
}(window.project.dropzoneCommon = window.project.dropzoneCommon || {}));