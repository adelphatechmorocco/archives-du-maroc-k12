﻿using System.Web.Mvc;

using Kentico.PageBuilder.Web.Mvc;
using Kentico.Web.Mvc;

using CMS.DocumentEngine.Types.AM;
using System.Globalization;
using ArchivesDuMaroc.Models;
using CMS.DocumentEngine;
using System.Linq;
using System;

namespace ArchivesDuMaroc.Controllers.Pages
{
    public class OutilsDeGestionDesArchivesController : BaseController
    {
        // GET: ArchivalTools
        public ActionResult Index()
        {
            ArchivalTools ArchivalToolsNode = ArchivalToolsProvider.GetArchivalTools("/ArchivalTools", CultureInfo.CurrentCulture.Name, _siteName)
                .Columns("DocumentName", "DocumentID", "NodeGUID", "ArchivalToolsTitle", "ArchivalToolsHeroImage", "ArchivalToolsText");

            if (ArchivalToolsNode == null)
                return HttpNotFound();

            var slider = DocumentHelper.GetDocuments<ArchivalToolsFiles>().Path("/ArchivalTools", PathTypeEnum.Children).Culture(CultureInfo.CurrentCulture.Name)
                .OrderBy("NodeOrder").Columns("NodeGUID", "DocumentCulture", "NodeAlias", "ArchivalToolsFile", "DocumentName", "DocumentPublishFrom", "DocumentPublishTo")
                .Where(m => (m.DocumentPublishFrom == null || m.DocumentPublishFrom <= DateTime.Now) && (m.DocumentPublishTo == null || m.DocumentPublishTo >= DateTime.Now)).ToList();

            var ArchivalToolsModel = new ArchivalToolsViewModel(ArchivalToolsNode, slider);
            HttpContext.Kentico().PageBuilder().Initialize(ArchivalToolsNode.DocumentID);
            return View(ArchivalToolsModel);
        }
    }
}