﻿using System.Web.Mvc;

using Kentico.PageBuilder.Web.Mvc;
using Kentico.Web.Mvc;

using CMS.DocumentEngine.Types.AM;
using System.Globalization;
using ArchivesDuMaroc.Models;

namespace ArchivesDuMaroc.Controllers.Pages
{
    public class ConfiezNousVosArchivesController : BaseController
    {
        // GET: Directeur
        public ActionResult Index()
        {
            EntrustArchives EntrustArchivesNode = EntrustArchivesProvider.GetEntrustArchives("/EntrustArchives", CultureInfo.CurrentCulture.Name, _siteName)
                .Columns("DocumentName", "DocumentID", "NodeGUID", "EntrustArchivesTitle", "EntrustArchivesHeroImage", "EntrustArchivesImage", "EntrustArchivesText", "EntrustArchivesQuote");
           
            if (EntrustArchivesNode == null)
                return HttpNotFound();

            var EntrustArchivesModel = new EntrustArchivesViewModel(EntrustArchivesNode);
            HttpContext.Kentico().PageBuilder().Initialize(EntrustArchivesNode.DocumentID);
            return View(EntrustArchivesModel);
        }
    }
}