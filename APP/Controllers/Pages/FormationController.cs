﻿using System.Web.Mvc;

using Kentico.PageBuilder.Web.Mvc;
using Kentico.Web.Mvc;

using CMS.DocumentEngine.Types.AM;
using System.Globalization;
using ArchivesDuMaroc.Models;

namespace ArchivesDuMaroc.Controllers.Pages
{
    public class FormationController : BaseController
    {
        // GET: Directeur
        public ActionResult Index()
        {
            ProfessionalTraining ProfessionalTrainingNode = ProfessionalTrainingProvider.GetProfessionalTraining("/ProfessionalTraining", CultureInfo.CurrentCulture.Name, _siteName)
                .Columns("DocumentName", "DocumentID", "NodeGUID", "ProfessionalTrainingTitle", "ProfessionalTrainingHeroImage");

            if (ProfessionalTrainingNode == null)
                return HttpNotFound();

            var ProfessionalTrainingModel = new ProfessionalTrainingViewModel(ProfessionalTrainingNode);
            HttpContext.Kentico().PageBuilder().Initialize(ProfessionalTrainingNode.DocumentID);
            return View(ProfessionalTrainingModel);
        }
    }
}