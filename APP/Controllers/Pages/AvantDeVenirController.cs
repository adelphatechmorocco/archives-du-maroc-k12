﻿using System.Web.Mvc;

using Kentico.PageBuilder.Web.Mvc;
using Kentico.Web.Mvc;

using CMS.DocumentEngine.Types.AM;
using System.Globalization;
using ArchivesDuMaroc.Models;

namespace ArchivesDuMaroc.Controllers.Pages
{
    public class AvantDeVenirController : BaseController
    {
        // GET: Directeur
        public ActionResult Index()
        {
            BeforeComing BeforeComingNode = BeforeComingProvider.GetBeforeComing("/BeforeComing", CultureInfo.CurrentCulture.Name, _siteName)
                .Columns("DocumentName", "DocumentID", "NodeGUID", "BeforeComingTitle", "BeforeComingHeroImage", "BeforeComingText");

            if (BeforeComingNode == null)
                return HttpNotFound();

            var BeforeComingModel = new BeforeComingViewModel(BeforeComingNode);
            HttpContext.Kentico().PageBuilder().Initialize(BeforeComingNode.DocumentID);
            return View(BeforeComingModel);
        }
    }
}