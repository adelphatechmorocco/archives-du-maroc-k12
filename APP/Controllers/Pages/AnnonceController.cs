﻿using System.Web.Mvc;

using Kentico.PageBuilder.Web.Mvc;
using Kentico.Web.Mvc;

using CMS.DocumentEngine.Types.AM;
using System.Globalization;
using ArchivesDuMaroc.Models;
using CMS.DocumentEngine;
using System.Linq;
using System;

namespace ArchivesDuMaroc.Controllers.Pages
{
    public class AnnonceController : BaseController
    {
        public ActionResult Recruitment()
        {
            Notice NoticeNode = NoticeProvider.GetNotice("/Recruitment", CultureInfo.CurrentCulture.Name, _siteName)
                .Columns("DocumentName", "DocumentID", "NodeGUID", "NoticeHeroTitle", "NoticeHeroImage");

            if (NoticeNode == null)
                return HttpNotFound();

            var slider = DocumentHelper.GetDocuments<NoticeItem>().Path("/Recruitment", PathTypeEnum.Children).Culture(CultureInfo.CurrentCulture.Name).OrderBy("NodeOrder")
                .Columns("NodeGUID", "DocumentCulture", "NodeAlias", "NoticeTitle", "NoticeNumberPlaces", "NoticeDatePublish", "NoticeDateContest", "NoticeDateDeadline",
                "NoticeSummonedWrittenExam", "NoticeSummonedOralExam", "NoticeResult", "NoticeCancellations", "DocumentPublishFrom", "DocumentPublishTo")
                .OrderBy("NoticeDatePublish").Where(m => (m.DocumentPublishFrom == null || m.DocumentPublishFrom <= DateTime.Now) && (m.DocumentPublishTo == null || m.DocumentPublishTo >= DateTime.Now)).ToList();

            var NoticeModel = new NoticeViewModel(NoticeNode, slider);

            HttpContext.Kentico().PageBuilder().Initialize(NoticeNode.DocumentID);
            return View("Notice", NoticeModel);
        }

        public ActionResult Candidature()
        {
            Notice NoticeNode = NoticeProvider.GetNotice("/Candidature", CultureInfo.CurrentCulture.Name, _siteName)
                .Columns("DocumentName", "DocumentID", "NodeGUID", "NoticeHeroTitle", "NoticeHeroImage");

            if (NoticeNode == null)
                return HttpNotFound();

            var slider = DocumentHelper.GetDocuments<NoticeItem>().Path("/Candidature", PathTypeEnum.Children).Culture(CultureInfo.CurrentCulture.Name).OrderBy("NodeOrder")
                .Columns("NodeGUID", "DocumentCulture", "NodeAlias", "NoticeTitle", "NoticeNumberPlaces", "NoticeDatePublish", "NoticeDateContest", "NoticeDateDeadline",
                "NoticeSummonedWrittenExam", "NoticeSummonedOralExam", "NoticeResult", "NoticeCancellations", "DocumentPublishFrom", "DocumentPublishTo")
                .OrderBy("NoticeDatePublish").Where(m => (m.DocumentPublishFrom == null || m.DocumentPublishFrom <= DateTime.Now) && (m.DocumentPublishTo == null || m.DocumentPublishTo >= DateTime.Now)).ToList();

            var NoticeModel = new NoticeViewModel(NoticeNode, slider);

            HttpContext.Kentico().PageBuilder().Initialize(NoticeNode.DocumentID);
            return View("Notice", NoticeModel);
        }
    }
}