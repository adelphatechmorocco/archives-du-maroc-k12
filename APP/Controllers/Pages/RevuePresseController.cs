﻿using System.Web.Mvc;

using Kentico.PageBuilder.Web.Mvc;
using Kentico.Web.Mvc;

using CMS.DocumentEngine.Types.AM;
using System.Globalization;
using ArchivesDuMaroc.Models;
using CMS.DocumentEngine;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace ArchivesDuMaroc.Controllers.Pages
{
    public class RevuePresseController : BaseController
    {
        // GET: Newspapers
        public ActionResult Index([FromUri]int[] filter)
        {
            Newspapers NewspapersNode = NewspapersProvider.GetNewspapers("/Newspapers", CultureInfo.CurrentCulture.Name, _siteName)
                .Columns("DocumentName", "DocumentID", "NodeGUID", "NewspapersHeroTitle", "NewspapersHeroImage");

            if (NewspapersNode == null)
                return HttpNotFound();

            var slider = DocumentHelper.GetDocuments<NewspaperItem>().Path("/Newspapers", PathTypeEnum.Children).Culture(CultureInfo.CurrentCulture.Name)
                .OrderBy("NodeOrder").Columns("NodeGUID", "DocumentCulture", "NodeAlias", "NewspaperTitle", "NewspaperImage", "NewspaperYear", "NewspaperYear", "NewspaperDescription", "NewspaperSource", "NewspaperFile", "DocumentPublishFrom", "DocumentPublishTo")
                .Where(m => (m.DocumentPublishFrom == null || m.DocumentPublishFrom <= DateTime.Now) && (m.DocumentPublishTo == null || m.DocumentPublishTo >= DateTime.Now)).ToList();
            
            List<int> yearsList = slider.Select(s => int.Parse(s.NewspaperYear)).Distinct().ToList();
            List<int> selectedYears = new List<int>();
            if (filter != null && filter.Any())
            {
                slider = slider.Where(s => filter.Contains(int.Parse(s.NewspaperYear))).ToList();
                selectedYears = filter.ToList();
            }
            var NewspapersModel = new NewspapersViewModel(NewspapersNode, slider, yearsList, selectedYears);

            HttpContext.Kentico().PageBuilder().Initialize(NewspapersNode.DocumentID);
            return View(NewspapersModel);
        }

        public ActionResult Details(Guid id)
        {
            Newspapers NewspapersNode = NewspapersProvider.GetNewspapers("/Newspapers", CultureInfo.CurrentCulture.Name, _siteName)
                .Columns("DocumentName", "DocumentID", "NodeGUID", "NewspapersHeroImage");

            if (NewspapersNode == null)
                return HttpNotFound();

            NewspaperItem newspaperItem = NewspaperItemProvider.GetNewspaperItem(id, CultureInfo.CurrentCulture.Name, _siteName)
                .Columns("DocumentName", "DocumentID", "NodeGUID", "DocumentCulture", "NodeAlias", "NewspaperTitle", "NewspaperImage", "NewspaperYear", "NewspaperYear", "NewspaperDescription", "NewspaperSource", "NewspaperFile", "DocumentPublishFrom", "DocumentPublishTo", "NodeParentID");

            if (newspaperItem == null)
                return HttpNotFound();

            var NewspaperItemModel = new NewspaperItemViewModel(newspaperItem, NewspapersNode.NewspapersHeroImage);

            HttpContext.Kentico().PageBuilder().Initialize(newspaperItem.DocumentID);
            return View(NewspaperItemModel);
        }
    }
}