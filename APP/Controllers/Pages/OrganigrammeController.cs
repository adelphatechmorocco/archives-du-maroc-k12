﻿using System.Web.Mvc;

using Kentico.PageBuilder.Web.Mvc;
using Kentico.Web.Mvc;

using CMS.DocumentEngine.Types.AM;
using System.Globalization;
using ArchivesDuMaroc.Models;

namespace ArchivesDuMaroc.Controllers.Pages
{
    public class OrganigrammeController : BaseController
    {
        // GET: Organigramme
        public ActionResult Index()
        {
            Organigramme OrganigrammeNode = OrganigrammeProvider.GetOrganigramme("/Organigramme", CultureInfo.CurrentCulture.Name, _siteName)
                .Columns("DocumentName", "DocumentID", "NodeGUID", "OrganigrammeTitle", "OrganigrammeHeroImage", "OrganigrammeImage");

            if (OrganigrammeNode == null)
                return HttpNotFound();

            var OrganigrammeModel = new OrganigrammeViewModel(OrganigrammeNode);
            HttpContext.Kentico().PageBuilder().Initialize(OrganigrammeNode.DocumentID);
            return View(OrganigrammeModel);
        }
    }
}