﻿using System.Web.Mvc;

using Kentico.PageBuilder.Web.Mvc;
using Kentico.Web.Mvc;

using CMS.DocumentEngine.Types.AM;
using System.Globalization;
using ArchivesDuMaroc.Models;

namespace ArchivesDuMaroc.Controllers.Pages
{
    public class EditoController : BaseController
    {
        // GET: Edito
        public ActionResult Index()
        {
            Edito editoNode = EditoProvider.GetEdito("/Edito", CultureInfo.CurrentCulture.Name, _siteName)
                .Columns("DocumentName", "DocumentID", "NodeGUID", "EditoTitle", "EditoHeroImage", "EditoImage", "EditoText", "EditoQuote");

            if (editoNode == null)
                return HttpNotFound();
            
            var homeModel = new EditoViewModel(editoNode);
            HttpContext.Kentico().PageBuilder().Initialize(editoNode.DocumentID);
            return View(homeModel);
        }
    }
}