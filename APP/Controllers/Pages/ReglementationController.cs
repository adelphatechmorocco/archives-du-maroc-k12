﻿using System.Web.Mvc;

using Kentico.PageBuilder.Web.Mvc;
using Kentico.Web.Mvc;

using CMS.DocumentEngine.Types.AM;
using System.Globalization;
using ArchivesDuMaroc.Models;
using CMS.DocumentEngine;
using System.Linq;
using System;

namespace ArchivesDuMaroc.Controllers.Pages
{
    public class ReglementationController : BaseController
    {
        // GET: Regulations
        public ActionResult Index()
        {
            Regulations RegulationsNode = RegulationsProvider.GetRegulations("/Regulation", CultureInfo.CurrentCulture.Name, _siteName)
                .Columns("DocumentName", "DocumentID", "NodeGUID", "RegulationsTitle", "RegulationsHeroImage", "RegulationsText");

            if (RegulationsNode == null)
                return HttpNotFound();

            var slider = DocumentHelper.GetDocuments<RegulationsFile>().Path("/Regulation", PathTypeEnum.Children).Culture(CultureInfo.CurrentCulture.Name)
                .OrderBy("NodeOrder").Columns("NodeGUID", "DocumentCulture", "NodeAlias", "RegulationFile", "DocumentName", "DocumentPublishFrom", "DocumentPublishTo")
                .Where(m => (m.DocumentPublishFrom == null || m.DocumentPublishFrom <= DateTime.Now) && (m.DocumentPublishTo == null || m.DocumentPublishTo >= DateTime.Now)).ToList();


            var RegulationsModel = new RegulationsViewModel(RegulationsNode, slider);
            HttpContext.Kentico().PageBuilder().Initialize(RegulationsNode.DocumentID);
            return View(RegulationsModel);
        }
    }
}