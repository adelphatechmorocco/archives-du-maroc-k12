﻿using System.Web.Mvc;

using Kentico.PageBuilder.Web.Mvc;
using Kentico.Web.Mvc;

using CMS.DocumentEngine.Types.AM;
using System.Globalization;
using ArchivesDuMaroc.Models;

namespace ArchivesDuMaroc.Controllers.Pages
{
    public class SalleConsultationController : BaseController
    {
        // GET: Directeur
        public ActionResult Index()
        {
            ConsultationRoom ConsultationRoomNode = ConsultationRoomProvider.GetConsultationRoom("/ConsultationRoom", CultureInfo.CurrentCulture.Name, _siteName)
                .Columns("DocumentName", "DocumentID", "NodeGUID", "ConsultationRoomTitle", "ConsultationRoomHeroImage", "ConsultationRoomImage", "ConsultationRoomText", "ConsultationRoomQuote", "FileRegulation", "FileSubscription", "TitleRegulation", "TitleSubscription");
           
            if (ConsultationRoomNode == null)
                return HttpNotFound();

            var ConsultationRoomModel = new ConsultationRoomViewModel(ConsultationRoomNode);
            HttpContext.Kentico().PageBuilder().Initialize(ConsultationRoomNode.DocumentID);
            return View(ConsultationRoomModel);
        }
    }
}