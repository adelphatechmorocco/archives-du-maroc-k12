﻿using System.Web.Mvc;

using Kentico.PageBuilder.Web.Mvc;
using Kentico.Web.Mvc;

using CMS.DocumentEngine.Types.AM;
using System.Globalization;
using ArchivesDuMaroc.Models;
using CMS.DocumentEngine;
using System.Linq;
using System;

namespace ArchivesDuMaroc.Controllers.Pages
{
    public class ProgrammePrevisionnelController : BaseController
    {
        // GET: ForecastProgram
        public ActionResult Index()
        {
            ForecastProgram ForecastProgramNode = ForecastProgramProvider.GetForecastProgram("/ForecastProgram", CultureInfo.CurrentCulture.Name, _siteName)
                .Columns("DocumentName", "DocumentID", "NodeGUID", "ForecastProgramHeroTitle", "ForecastProgramHeroImage");

            if (ForecastProgramNode == null)
                return HttpNotFound();

            var slider = DocumentHelper.GetDocuments<ForecastItem>().Path("/ForecastProgram", PathTypeEnum.Children).Culture(CultureInfo.CurrentCulture.Name)
                .OrderBy("NodeOrder").Columns("NodeGUID", "DocumentCulture", "NodeAlias", "ForecastItemTitle", "ForecastItemBudgetYear", "ForecastItemFile", "DocumentPublishFrom", "DocumentPublishTo")
                .Where(m => (m.DocumentPublishFrom == null || m.DocumentPublishFrom <= DateTime.Now) && (m.DocumentPublishTo == null || m.DocumentPublishTo >= DateTime.Now)).ToList();

            var ForecastProgramModel = new ForecastProgramViewModel(ForecastProgramNode, slider);

            HttpContext.Kentico().PageBuilder().Initialize(ForecastProgramNode.DocumentID);
            return View(ForecastProgramModel);
        }
    }
}