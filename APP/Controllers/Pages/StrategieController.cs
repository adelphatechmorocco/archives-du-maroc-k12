﻿using System.Web.Mvc;

using Kentico.PageBuilder.Web.Mvc;
using Kentico.Web.Mvc;

using CMS.DocumentEngine.Types.AM;
using System.Globalization;
using ArchivesDuMaroc.Models;

namespace ArchivesDuMaroc.Controllers.Pages
{
    public class StrategieController : BaseController
    {
        // GET: Directeur
        public ActionResult Index()
        {
            Strategy StrategyNode = StrategyProvider.GetStrategy("/Strategy", CultureInfo.CurrentCulture.Name, _siteName)
                .Columns("DocumentName", "DocumentID", "NodeGUID", "StrategyTitle", "StrategyHeroImage", "StrategyText");

            if (StrategyNode == null)
                return HttpNotFound();

            var StrategyModel = new StrategyViewModel(StrategyNode);
            HttpContext.Kentico().PageBuilder().Initialize(StrategyNode.DocumentID);
            return View(StrategyModel);
        }
    }
}