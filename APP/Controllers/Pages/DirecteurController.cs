﻿using System.Web.Mvc;

using Kentico.PageBuilder.Web.Mvc;
using Kentico.Web.Mvc;

using CMS.DocumentEngine.Types.AM;
using System.Globalization;
using ArchivesDuMaroc.Models;

namespace ArchivesDuMaroc.Controllers.Pages
{
    public class DirecteurController : BaseController
    {
        // GET: Directeur
        public ActionResult Index()
        {
            Director directorNode = DirectorProvider.GetDirector("/Director", CultureInfo.CurrentCulture.Name, _siteName)
                .Columns("DocumentName", "DocumentID", "NodeGUID", "DirectorTitle", "DirectorHeroImage", "DirectorImage", "DirectorText", "DirectorImageTitle");
           
            if (directorNode == null)
                return HttpNotFound();

            var directorModel = new DirectorViewModel(directorNode);
            HttpContext.Kentico().PageBuilder().Initialize(directorNode.DocumentID);
            return View(directorModel);
        }
    }
}