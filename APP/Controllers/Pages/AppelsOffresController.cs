﻿using System.Web.Mvc;

using Kentico.PageBuilder.Web.Mvc;
using Kentico.Web.Mvc;

using CMS.DocumentEngine.Types.AM;
using System.Globalization;
using ArchivesDuMaroc.Models;
using CMS.DocumentEngine;
using System.Linq;
using System.Collections.Generic;
using System;

namespace ArchivesDuMaroc.Controllers.Pages
{
    public class AppelsOffresController : BaseController
    {
        // GET: Tenders
        public ActionResult Index()
        {
            Tenders TendersNode = TendersProvider.GetTenders("/AppelsOffres", CultureInfo.CurrentCulture.Name, _siteName)
                .Columns("DocumentName", "DocumentID", "NodeGUID", "TendersHeroTitle", "TendersHeroImage");

            if (TendersNode == null)
                return HttpNotFound();

            var slider = DocumentHelper.GetDocuments<TendersItem>().Path("/AppelsOffres", PathTypeEnum.Children).Culture(CultureInfo.CurrentCulture.Name)
                .OrderBy("NodeOrder").Columns("NodeGUID", "NodeID", "DocumentCulture", "NodeAlias", "TendersItemTitle", "TendersItemText", "DocumentPublishFrom", "DocumentPublishTo")
                .Where(m => (m.DocumentPublishFrom == null || m.DocumentPublishFrom <= DateTime.Now) && (m.DocumentPublishTo == null || m.DocumentPublishTo >= DateTime.Now)).ToList();

            var items = new List<TendersItemViewModel>();
            foreach(var item in slider)
            {
                var files = TendersFilesProvider.GetTendersFiles().Culture(CultureInfo.CurrentCulture.Name).OnSite(_siteName)
                    .Where(m => m.NodeParentID == item.NodeID && (m.DocumentPublishFrom == null || m.DocumentPublishFrom <= DateTime.Now) && (m.DocumentPublishTo == null || m.DocumentPublishTo >= DateTime.Now)).ToList();
                items.Add(new TendersItemViewModel(item, files));
            }

            var TendersModel = new TendersViewModel(TendersNode, items);

            HttpContext.Kentico().PageBuilder().Initialize(TendersNode.DocumentID);
            return View(TendersModel);
        }
    }
}