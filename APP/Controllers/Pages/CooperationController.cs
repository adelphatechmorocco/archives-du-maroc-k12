﻿using System.Web.Mvc;

using Kentico.PageBuilder.Web.Mvc;
using Kentico.Web.Mvc;

using CMS.DocumentEngine.Types.AM;
using System.Globalization;
using ArchivesDuMaroc.Models;
using CMS.DocumentEngine;
using System.Linq;
using System;

namespace ArchivesDuMaroc.Controllers.Pages
{
    public class CooperationController : BaseController
    {
        // GET: Cooperation
        public ActionResult Index()
        {
            Cooperation CooperationNode = CooperationProvider.GetCooperation("/Cooperation", CultureInfo.CurrentCulture.Name, _siteName)
                .Columns("DocumentName", "DocumentID", "NodeGUID", "CooperationTitle", "CooperationHeroImage", "CooperationText");

            if (CooperationNode == null)
                return HttpNotFound();

            var slider = DocumentHelper.GetDocuments<SliderCooperation>().Path("/Cooperation", PathTypeEnum.Children).Culture(CultureInfo.CurrentCulture.Name)
                .OrderBy("NodeOrder").Columns("NodeGUID", "DocumentCulture", "NodeAlias", "SliderCooperationImage", "DocumentPublishFrom", "DocumentPublishTo")
                .Where(m => (m.DocumentPublishFrom == null || m.DocumentPublishFrom <= DateTime.Now) && (m.DocumentPublishTo == null || m.DocumentPublishTo >= DateTime.Now)).ToList();

            var CooperationModel = new CooperationViewModel(CooperationNode, slider);
            HttpContext.Kentico().PageBuilder().Initialize(CooperationNode.DocumentID);
            return View(CooperationModel);
        }
    }
}