﻿using System.Web.Mvc;

using Kentico.PageBuilder.Web.Mvc;
using Kentico.Web.Mvc;

using CMS.DocumentEngine.Types.AM;
using System.Globalization;
using ArchivesDuMaroc.Models;
using CMS.DocumentEngine;
using System.Linq;
using System;

namespace ArchivesDuMaroc.Controllers.Pages
{
    public class HoraireOuvertureController : BaseController
    {
        // GET: Directeur
        public ActionResult Index()
        {
            PageOpeningHours PageOpeningHoursNode = PageOpeningHoursProvider.GetPageOpeningHours("/PageOpeningHours", CultureInfo.CurrentCulture.Name, _siteName)
                .Columns("DocumentName", "DocumentID", "NodeGUID", "PageOpeningHoursTitle", "PageOpeningHoursHeroImage", "PageOpeningHoursText");

            if (PageOpeningHoursNode == null)
                return HttpNotFound();

            var slider = DocumentHelper.GetDocuments<OpeningHoursItems>().Path("/PageOpeningHours", PathTypeEnum.Children).Culture(CultureInfo.CurrentCulture.Name)
                .OrderBy("NodeOrder").Columns("NodeGUID", "DocumentCulture", "NodeAlias", "OpeningHoursItemsServiceTitle", "OpeningHoursItemsServiceHours", "OpeningHoursItemsServicClosing", "DocumentPublishFrom", "DocumentPublishTo")
                .Where(m => (m.DocumentPublishFrom == null || m.DocumentPublishFrom <= DateTime.Now) && (m.DocumentPublishTo == null || m.DocumentPublishTo >= DateTime.Now)).ToList();

            var PageOpeningHoursModel = new PageOpeningHoursViewModel(PageOpeningHoursNode, slider);
            HttpContext.Kentico().PageBuilder().Initialize(PageOpeningHoursNode.DocumentID);
            return View(PageOpeningHoursModel);
        }
    }
}