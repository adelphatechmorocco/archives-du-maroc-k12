﻿using System.Web.Mvc;

using Kentico.PageBuilder.Web.Mvc;
using Kentico.Web.Mvc;

using CMS.DocumentEngine.Types.AM;
using System.Globalization;
using ArchivesDuMaroc.Models;

namespace ArchivesDuMaroc.Controllers.Pages
{
    public class ContactezNousController : BaseController
    {
        // GET: Directeur
        public ActionResult Index()
        {
            ContactUs ContactUsNode = ContactUsProvider.GetContactUs("/ContactUs", CultureInfo.CurrentCulture.Name, _siteName)
                .Columns("DocumentName", "DocumentID", "NodeGUID", "ContactUsTitle", "ContactUsHeroImage");
            Master master = MasterProvider.GetMaster("/Master", CultureInfo.CurrentCulture.Name, _siteName)
               .Columns("Location");
            if (ContactUsNode == null || master == null)
                return HttpNotFound();

            var ContactUsModel = new ContactUsViewModel(ContactUsNode, master.Location);
            HttpContext.Kentico().PageBuilder().Initialize(ContactUsNode.DocumentID);
            return View(ContactUsModel);
        }
    }
}