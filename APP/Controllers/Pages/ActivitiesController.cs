﻿using System.Web.Mvc;

using Kentico.PageBuilder.Web.Mvc;
using Kentico.Web.Mvc;

using CMS.DocumentEngine.Types.AM;
using System.Globalization;
using ArchivesDuMaroc.Models;
using CMS.DocumentEngine;
using System.Linq;
using System.Collections.Generic;
using System;

namespace ArchivesDuMaroc.Controllers.Pages
{
    public class ActivitiesController : BaseController
    {
        // GET: Activitys
        public ActionResult Index()
        {
            Activities activitiesNode = ActivitiesProvider.GetActivities("/Activities", CultureInfo.CurrentCulture.Name, _siteName)
                .Columns("DocumentName",  "DocumentID", "ActivitiesTitle", "ActivitiesHeroImage", "NodeGUID");

            if (activitiesNode == null)
                return HttpNotFound();

            var activities = ActivityProvider.GetActivities().Culture(CultureInfo.CurrentCulture.Name).OnSite(_siteName)
                .OrderByDescending("DocumentCreatedWhen").Columns("DocumentName", "DocumentID", "DocumentCulture", "NodeAliasPath", "NodeAlias", 
                "ActivityMainTitle", "ActivitySubTitle", "ActivityMainImage", "ActivityDateStart", "ActivityDatend", "ActivityTime", 
                "ActivityLocation", "ActivityText")
                .Select(a => new ActivityViewModel(a, new List<ActivityFiles>(), new List<ActivityImages>())).ToList();

            if (!activities.Any())
                return HttpNotFound();

            var activitiesModel = new ActivitiesViewModel(activitiesNode, activities);

            HttpContext.Kentico().PageBuilder().Initialize(activitiesNode.DocumentID);

            return View(activitiesModel);
        }

        public ActionResult Details(string id)
        {
            Activity ActivityNode = ActivityProvider.GetActivity("/Activities/" + id, CultureInfo.CurrentCulture.Name, _siteName)
                .Columns("DocumentName", "DocumentID", "ActivityTitle", "ActivityHeroImage", "ActivityMainTitle", "ActivitySubTitle", "ActivityMainImage", "ActivityDateStart",
                "ActivityDatend", "ActivityTime", "ActivityLocation", "ActivityText", "ActivityTags", "NodeGUID");

            if (ActivityNode == null)
                return HttpNotFound();

            var sliderImages = DocumentHelper.GetDocuments<ActivityImages>().Path("/Activities/" + id + "/Images", PathTypeEnum.Children).Culture(CultureInfo.CurrentCulture.Name)
                .OrderBy("NodeOrder").Columns("NodeGUID", "DocumentCulture", "NodeAlias", "ActivityImage", "DocumentName", "DocumentPublishFrom", "DocumentPublishTo")
                .Where(m => (m.DocumentPublishFrom == null || m.DocumentPublishFrom <= DateTime.Now) && (m.DocumentPublishTo == null || m.DocumentPublishTo >= DateTime.Now)).ToList();

            var sliderFiles = DocumentHelper.GetDocuments<ActivityFiles>().Path("/Activities/" + id + "/Files", PathTypeEnum.Children).Culture(CultureInfo.CurrentCulture.Name)
                .OrderBy("NodeOrder").Columns("NodeGUID", "DocumentCulture", "NodeAlias", "ActivityFile", "DocumentName", "DocumentPublishFrom", "DocumentPublishTo")
                .Where(m => (m.DocumentPublishFrom == null || m.DocumentPublishFrom <= DateTime.Now) && (m.DocumentPublishTo == null || m.DocumentPublishTo >= DateTime.Now)).ToList();

            var ActivityModel = new ActivityViewModel(ActivityNode, sliderFiles, sliderImages);

            HttpContext.Kentico().PageBuilder().Initialize(ActivityNode.DocumentID);
            return View(ActivityModel);
        }

        public ActionResult Tag(string id)
        {
            ViewBag.Tag = id;
            Activities activitiesNode = ActivitiesProvider.GetActivities("/Activities", CultureInfo.CurrentCulture.Name, _siteName)
                .Columns("DocumentName", "DocumentID", "ActivitiesTitle", "ActivitiesHeroImage", "NodeGUID");

            if(activitiesNode == null)
                return HttpNotFound();

            var activities = ActivityProvider.GetActivities().Culture(CultureInfo.CurrentCulture.Name).OnSite(_siteName).WhereContains("ActivityTags", id)
                .OrderByDescending("DocumentCreatedWhen").Columns("DocumentName", "DocumentID", "DocumentCulture", "NodeAliasPath", "NodeAlias",
                "ActivityMainTitle", "ActivitySubTitle", "ActivityMainImage", "ActivityDateStart", "ActivityDatend", "ActivityTime",
                "ActivityLocation", "ActivityText")
                .Select(a => new ActivityViewModel(a, new List<ActivityFiles>(), new List<ActivityImages>())).ToList();

            if (!activities.Any())
                return HttpNotFound();

            var activitiesModel = new ActivitiesViewModel(activitiesNode, activities);

            HttpContext.Kentico().PageBuilder().Initialize(activitiesNode.DocumentID);

            return View(activitiesModel);
        }
    }
}