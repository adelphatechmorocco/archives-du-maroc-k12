﻿using System.Web.Mvc;

using Kentico.PageBuilder.Web.Mvc;
using Kentico.Web.Mvc;

using CMS.DocumentEngine.Types.AM;
using System.Globalization;
using ArchivesDuMaroc.Models;

namespace ArchivesDuMaroc.Controllers.Pages
{
    public class PresentationController : BaseController
    {
        // GET: Presentation
        public ActionResult Index()
        {
            Presentation PresentationNode = PresentationProvider.GetPresentation("/Presentation", CultureInfo.CurrentCulture.Name, _siteName)
                .Columns("DocumentName", "DocumentID", "NodeGUID", "PresentationTitle", "PresentationHeroImage", "PresentationImage", "PresentationText");

            if (PresentationNode == null)
                return HttpNotFound();

            var PresentationModel = new PresentationViewModel(PresentationNode);
            HttpContext.Kentico().PageBuilder().Initialize(PresentationNode.DocumentID);
            return View(PresentationModel);
        }
    }
}