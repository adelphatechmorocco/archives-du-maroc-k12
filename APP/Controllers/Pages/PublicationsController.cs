﻿using System.Web.Mvc;

using Kentico.PageBuilder.Web.Mvc;
using Kentico.Web.Mvc;

using CMS.DocumentEngine.Types.AM;
using System.Globalization;
using ArchivesDuMaroc.Models;
using CMS.DocumentEngine;
using System.Linq;
using System;

namespace ArchivesDuMaroc.Controllers.Pages
{
    public class PublicationsController : BaseController
    {
        // GET: Publications
        public ActionResult Index()
        {
            Publications PublicationsNode = PublicationsProvider.GetPublications("/Publications", CultureInfo.CurrentCulture.Name, _siteName)
                .Columns("DocumentName", "DocumentID", "NodeGUID", "PublicationsTitle", "PublicationsHeroImage");

            if (PublicationsNode == null)
                return HttpNotFound();

            var slider = DocumentHelper.GetDocuments<PublicationItems>().Path("/Publications", PathTypeEnum.Children).Culture(CultureInfo.CurrentCulture.Name)
                .OrderBy("NodeOrder").Columns("NodeGUID", "DocumentCulture", "NodeAlias", "PublicationTitle", "PublicationImage", "PublicationText", "PublicationFile", "DocumentPublishFrom", "DocumentPublishTo")
                .Where(m => (m.DocumentPublishFrom == null || m.DocumentPublishFrom <= DateTime.Now) && (m.DocumentPublishTo == null || m.DocumentPublishTo >= DateTime.Now)).ToList();

            var PublicationsModel = new PublicationsViewModel(PublicationsNode, slider);

            HttpContext.Kentico().PageBuilder().Initialize(PublicationsNode.DocumentID);
            return View(PublicationsModel);
        }

        public ActionResult Details(Guid id)
        {
            PublicationItems PublicationItem = PublicationItemsProvider.GetPublicationItems(id, CultureInfo.CurrentCulture.Name, _siteName)
                .Columns("DocumentName", "DocumentID", "NodeGUID", "DocumentCulture", "NodeAlias", "PublicationTitle", "PublicationHeroImage", "PublicationReferences", "PublicationImage", "PublicationText", "PublicationFile", "NodeParentID");

            if (PublicationItem == null)
                return HttpNotFound();

            var PublicationItemModel = new PublicationItemViewModel(PublicationItem);

            HttpContext.Kentico().PageBuilder().Initialize(PublicationItem.DocumentID);
            return View(PublicationItemModel);
        }
    }
}