﻿using System.Web.Mvc;

using Kentico.PageBuilder.Web.Mvc;
using Kentico.Web.Mvc;

using CMS.DocumentEngine.Types.AM;
using System.Globalization;
using ArchivesDuMaroc.Models;

namespace ArchivesDuMaroc.Controllers.Pages
{
    public class AssistanceTechniqueController : BaseController
    {
        // GET: Directeur
        public ActionResult Index()
        {
            TechnicalAssistance TechnicalAssistanceNode = TechnicalAssistanceProvider.GetTechnicalAssistance("/TechnicalAssistance", CultureInfo.CurrentCulture.Name, _siteName)
                .Columns("DocumentName", "DocumentID", "NodeGUID", "TechnicalAssistanceTitle", "TechnicalAssistanceHeroImage");

            if (TechnicalAssistanceNode == null)
                return HttpNotFound();

            var TechnicalAssistanceModel = new TechnicalAssistanceViewModel(TechnicalAssistanceNode);
            HttpContext.Kentico().PageBuilder().Initialize(TechnicalAssistanceNode.DocumentID);
            return View(TechnicalAssistanceModel);
        }
    }
}