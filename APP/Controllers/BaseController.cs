﻿using System.Linq;
using System.Web.Mvc;

using CMS.DocumentEngine;
using ArchivesDuMaroc.Models.Menu;
using ArchivesDuMaroc.Config;
using System.Globalization;
using CMS.DocumentEngine.Types.AM;
using System.Collections.Generic;
using ArchivesDuMaroc.Models.Base;
using System;

namespace ArchivesDuMaroc.Controllers
{
    public class BaseController : Controller
    {
        protected readonly string _siteName = AppConfig.Sitename;

        public ActionResult GetMenu()
        {
            var menuItems = DocumentHelper.GetDocuments<MenuItem>().Path("/Menu-content", PathTypeEnum.Children)
                .Culture(CultureInfo.CurrentCulture.Name).OrderBy("NodeOrder").Where(m => m.IsPublished && m.NodeLevel == 2).ToList();

            var pages = DocumentHelper.GetDocuments().Culture(CultureInfo.CurrentCulture.Name).OnSite(_siteName)
                .WhereIn("NodeGUID", menuItems.Select(item => item.MenuItemPage).ToList())
                .Columns("NodeGUID", "DocumentCulture", "NodeAlias");
            var models = new List<MenuItemViewModel>();

            foreach (var item in menuItems)
            {
                models.Add(new MenuItemViewModel
                {
                    MenuItemText = item.MenuItemText,
                    MenuItemRelativeUrl = pages.FirstOrDefault(page => page.NodeGUID == item.MenuItemPage)?.RelativeURL,
                    Children = item.Children
                });
            }

            Master master = MasterProvider.GetMaster("/Master", CultureInfo.CurrentCulture.Name, _siteName)
                .Columns("DocumentName", "DocumentID", "Logo");

            ViewBag.Logo = master.Logo;

            return PartialView("_SiteMenu", models);
        }

        public ActionResult GetFooter()
        {
            Master master = MasterProvider.GetMaster("/Master", CultureInfo.CurrentCulture.Name, _siteName)
                .Columns("DocumentName", "DocumentID", "CompanyName", "Address", "Phone", "Fax", "Email", "Location");

            var openingHours = DocumentHelper.GetDocuments<OpeningHours>().Path("/Master/Horaire-d-ouvertures", PathTypeEnum.Children).Culture(CultureInfo.CurrentCulture.Name)
                .OrderBy("NodeOrder").Columns("NodeGUID", "DocumentCulture", "NodeAlias", "Day", "TimeStart", "TimeEnd", "Closed").ToList();

            var socialMedias = DocumentHelper.GetDocuments<SocialMedia>().Path("/Master/Reseaux-sociaux", PathTypeEnum.Children).Culture(CultureInfo.CurrentCulture.Name)
                .OrderBy("NodeOrder").Columns("NodeGUID", "DocumentCulture", "NodeAlias", "LinkIcon", "Link").ToList();

            var menuItems = DocumentHelper.GetDocuments<MenuItem>().Path("/Menu-content", PathTypeEnum.Children).Culture(CultureInfo.CurrentCulture.Name).WhereTrue("DisplayInFooter").OrderBy("NodeOrder");

            var pages = DocumentHelper.GetDocuments().Culture(CultureInfo.CurrentCulture.Name).OnSite(_siteName)
                .WhereIn("NodeGUID", menuItems.Select(item => item.MenuItemPage).ToList())
                .Columns("NodeGUID", "DocumentCulture", "NodeAlias");
            var menuItemModel = new List<MenuItemViewModel>();

            foreach (var item in menuItems)
            {
                menuItemModel.Add(new MenuItemViewModel
                {
                    MenuItemText = item.MenuItemText,
                    MenuItemRelativeUrl = pages.FirstOrDefault(page => page.NodeGUID == item.MenuItemPage)?.RelativeURL,
                    Children = item.Children
                });
            }

            FooterViewModel model = new FooterViewModel
            {
                master = master,
                openingHours = openingHours,
                socialMedias = socialMedias,
                MenuItems = menuItemModel
            };
            return PartialView("_SiteFooter", model);
        }

        public ActionResult GetLastAds()
        {
            var lastAds = DocumentHelper.GetDocuments<LastAds>().Path("/Master/Last-Ads-Folder", PathTypeEnum.Children).Culture(CultureInfo.CurrentCulture.Name)
                .OrderBy("NodeOrder").Columns("NodeGUID", "DocumentCulture", "NodeAlias", "Text", "DocumentPublishFrom", "DocumentPublishTo")
                .Where(m => (m.DocumentPublishFrom == null || m.DocumentPublishFrom <= DateTime.Now) && (m.DocumentPublishTo == null || m.DocumentPublishTo >= DateTime.Now)).ToList(); ;
            
            var models = new List<string>();

            foreach (var item in lastAds)
                models.Add(item.Text);
            

            return PartialView("_SiteCarousel", models);
        }
    }
}