﻿using System.Web.Mvc;

using Kentico.PageBuilder.Web.Mvc;
using Kentico.Web.Mvc;

using CMS.DocumentEngine.Types.AM;
using System.Globalization;
using CMS.DocumentEngine;
using System.Linq;
using ArchivesDuMaroc.Models;
using System;

namespace ArchivesDuMaroc.Controllers
{
    public class AccueilController : BaseController
    {
        // GET: Home
        public ActionResult Index()
        {
            Home homeNode = HomeProvider.GetHome("/Home", CultureInfo.CurrentCulture.Name, _siteName)
                .Columns("DocumentName", "DocumentID", "LinkYoutube", "VideoCategory", "VideoTitle", "VideoDescription");

            if (homeNode == null)
                return HttpNotFound();
            var slider = DocumentHelper.GetDocuments<Slider>().Path("/Home/Slider", PathTypeEnum.Children).Culture(CultureInfo.CurrentCulture.Name)
                .OrderBy("NodeOrder").Columns("NodeGUID", "DocumentCulture", "NodeAlias", "Image", "Title", "Description", "DocumentPublishFrom", "DocumentPublishTo")
                .Where(m=> (m.DocumentPublishFrom == null || m.DocumentPublishFrom <= DateTime.Now) && ( m.DocumentPublishTo == null || m.DocumentPublishTo >= DateTime.Now)).ToList();

            var lastPublication = DocumentHelper.GetDocuments<PublicationItems>().Path("/Publications", PathTypeEnum.Children).Culture(CultureInfo.CurrentCulture.Name).OnSite(_siteName)
                .Columns("NodeGUID", "DocumentCulture", "NodeAlias", "PublicationTitle", "PublicationImage", "PublicationText", "NodeParentID")
                .OrderByDescending("DocumentCreatedWhen").LastOrDefault();

            var lastPublicationModel = new PublicationItemViewModel(lastPublication);

            var homeModel = new HomeViewModel(homeNode, slider, lastPublicationModel);
            HttpContext.Kentico().PageBuilder().Initialize(homeNode.DocumentID);
            return View(homeModel);
        }
    }
}