﻿using System.Web.Mvc;

namespace ArchivesDuMaroc.Controllers.Sections
{
    public class SingleColumnSectionController : Controller
    {
        // GET: SingleColumnSection
        public ActionResult Index()
        {
            return PartialView("Sections/_SingleColumnSection");
        }
    }
}