﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ArchivesDuMaroc.Controllers.Sections
{
    public class TwoColumnHomeSectionController : Controller
    {
        // GET: FourColumnSection
        public ActionResult Index()
        {
            return PartialView("Sections/_TwoColumnHomeSection");
        }
    }
}