﻿using System.Linq;
using System.Web.Mvc;

using CMS.DocumentEngine;

using ArchivesDuMaroc.Controllers.Widgets;
using ArchivesDuMaroc.Models.Widgets;

using Kentico.PageBuilder.Web.Mvc;
using ArchivesDuMaroc.Models.Widgets.ReviewWidget;

namespace ArchivesDuMaroc.Controllers.Widgets
{
    public class ReviewWidgetController : WidgetController<ReviewWidgetProperties>
    {
        public ReviewWidgetController()
        {
        }

        public ReviewWidgetController(IWidgetPropertiesRetriever<ReviewWidgetProperties> propertiesRetriever,
                                        ICurrentPageRetriever currentPageRetriever) : base(propertiesRetriever, currentPageRetriever)
        {
        }

        public ActionResult Index()
        {
            var properties = GetProperties();
            var image = GetImage(properties);

            return PartialView("Widgets/_ReviewWidget", new ReviewWidgetViewModel
            {
                Category = properties.Category,
                Color = properties.Color,
                Image = image,
                Title = properties.Title,
                Description = properties.Description,
                CTALink = properties.CTALink,
                CTATarget = properties.CTATarget
            });
        }

        private DocumentAttachment GetImage(ReviewWidgetProperties properties)
        {
            var page = GetPage();
            return page?.AllAttachments.FirstOrDefault(x => x.AttachmentGUID == properties.ImageGuid);
        }
    }
}