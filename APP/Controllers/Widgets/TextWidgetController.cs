﻿using System.Web.Mvc;
using ArchivesDuMaroc.Models.Widgets.TextWidget;
using Kentico.PageBuilder.Web.Mvc;

namespace ArchivesDuMaroc.Controllers.Widgets
{
    public class TextWidgetController : WidgetController<TextWidgetProperties>
    {
        public ActionResult Index()
        {
            var properties = GetProperties();

            return PartialView("Widgets/_TextWidget", new TextWidgetViewModel { Text = properties.Text });
        }
    }
}