﻿using System.Linq;
using System.Web.Mvc;

using CMS.DocumentEngine;

using Kentico.PageBuilder.Web.Mvc;
using ArchivesDuMaroc.Models.Widgets.TrainingWidget;
namespace ArchivesDuMaroc.Controllers.Widgets
{
    public class TrainingWidgetController : WidgetController<TrainingWidgetProperties>
    {
        public TrainingWidgetController()
        {
        }

        public TrainingWidgetController(IWidgetPropertiesRetriever<TrainingWidgetProperties> propertiesRetriever,
                                        ICurrentPageRetriever currentPageRetriever) : base(propertiesRetriever, currentPageRetriever)
        {
        }

        public ActionResult Index()
        {
            var properties = GetProperties();
            var image = GetImage(properties);

            return PartialView("Widgets/_TrainingWidget", new TrainingWidgetViewModel
            {
                Image = image,
                Title = properties.Title,
                Description = properties.Description,
                Phone = properties.Phone,
                Email = properties.Email,
                Mission = properties.Mission
            });
        }

        private DocumentAttachment GetImage(TrainingWidgetProperties properties)
        {
            var page = GetPage();
            return page?.AllAttachments.FirstOrDefault(x => x.AttachmentGUID == properties.ImageGuid);
        }
    }
}