﻿using System.Linq;
using System.Web.Mvc;

using CMS.DocumentEngine;

using ArchivesDuMaroc.Models.Widgets.ToolboxWidget;

using Kentico.PageBuilder.Web.Mvc;

namespace ArchivesDuMaroc.Controllers.Widgets
{
    public class ToolboxWidgetController : WidgetController<ToolboxWidgetProperties>
    {
        // GET: Toolbox
        public ActionResult Index()
        {
            var properties = GetProperties();

            return PartialView("Widgets/_ToolboxWidget", new ToolboxWidgetViewModel
            {
                Title = properties.Title,
                Description = properties.Description,
                CTALink = properties.CTALink,
                Icon = properties.Icon
            });
        }
    }
}