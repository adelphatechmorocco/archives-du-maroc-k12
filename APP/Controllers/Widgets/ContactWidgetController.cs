﻿using System.Web.Mvc;

using Kentico.PageBuilder.Web.Mvc;
using ArchivesDuMaroc.Models.Widgets.ContactWidget;

namespace ArchivesDuMaroc.Controllers.Widgets
{
    public class ContactWidgetController : WidgetController<ContactWidgetProperties>
    {
        public ContactWidgetController()
        {
        }

        public ContactWidgetController(IWidgetPropertiesRetriever<ContactWidgetProperties> propertiesRetriever,
                                        ICurrentPageRetriever currentPageRetriever) : base(propertiesRetriever, currentPageRetriever)
        {
        }

        public ActionResult Index()
        {
            var properties = GetProperties();

            return PartialView("Widgets/_ContactWidget", new ContactWidgetViewModel
            {
                Title = properties.Title,
                Description = properties.Description,
                Phone = properties.Phone,
                Email = properties.Email,
                Text = properties.Text
            });
        }
    }
}