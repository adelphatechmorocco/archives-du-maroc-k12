﻿using System.Linq;
using System.Web.Mvc;

using CMS.DocumentEngine;

using ArchivesDuMaroc.Controllers.Widgets;
using ArchivesDuMaroc.Models.Widgets;

using Kentico.PageBuilder.Web.Mvc;
using ArchivesDuMaroc.Models.Widgets.CardWidget;

namespace ArchivesDuMaroc.Controllers.Widgets
{
    public class CardWidgetController : WidgetController<CardWidgetProperties>
    {
        public CardWidgetController()
        {
        }

        public CardWidgetController(IWidgetPropertiesRetriever<CardWidgetProperties> propertiesRetriever,
                                        ICurrentPageRetriever currentPageRetriever) : base(propertiesRetriever, currentPageRetriever)
        {
        }

        public ActionResult Index()
        {
            var properties = GetProperties();
            var image = GetImage(properties);

            return PartialView("Widgets/_CardWidget", new CardWidgetViewModel
            {
                Category = properties.Category,
                Color = properties.Color,
                Image = image,
                Title = properties.Title,
                Description = properties.Description,
                CTALink = properties.CTALink,
                CTATarget = properties.CTATarget
            });
        }

        private DocumentAttachment GetImage(CardWidgetProperties properties)
        {
            var page = GetPage();
            return page?.AllAttachments.FirstOrDefault(x => x.AttachmentGUID == properties.ImageGuid);
        }
    }
}