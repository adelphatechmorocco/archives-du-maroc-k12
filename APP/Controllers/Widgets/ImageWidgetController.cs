﻿using System.Linq;
using System.Web.Mvc;
using ArchivesDuMaroc.Infrastructure;
using ArchivesDuMaroc.Models.Widgets.ImageWidget;
using CMS.DocumentEngine;
using Kentico.PageBuilder.Web.Mvc;

namespace ArchivesDuMaroc.Controllers.Widgets
{
    public class ImageWidgetController : WidgetController<ImageWidgetProperties>
    {
        public ActionResult Index()
        {
            var properties = GetProperties();
            var image = GetImage(properties);

            return PartialView("Widgets/_ImageWidget", new ImageWidgetViewModel
            {
                Image = image
            });
        }


        private DocumentAttachment GetImage(ImageWidgetProperties properties)
        {
            var page = GetPage();
            return page?.AllAttachments.FirstOrDefault(x => x.AttachmentGUID == properties.ImageGuid);

        }
    }
}