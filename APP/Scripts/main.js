(function ($) {
    "use strict";

    $(function () {
        var header = $(".start-style");
        $(window).scroll(function () {
            var scroll = $(window).scrollTop();

            if (scroll >= 10) {
                header.removeClass('start-style').addClass("scroll-on");
            } else {
                header.removeClass("scroll-on").addClass('start-style');
            }
        });
    });

    //Switch light/dark

    $("#switch").on('click', function () {
        if ($("body").hasClass("dark")) {
            $("body").removeClass("dark");
            $("#switch").removeClass("switched");
        }
        else {
            $("body").addClass("dark");
            $("#switch").addClass("switched");
        }

    });

})(jQuery);
//coperation slide
function myFunction(imgs) {
    var expandImg = document.getElementById("expandedImg");
    var imgText = document.getElementById("imgtext");
    expandImg.src = imgs.src;
    expandImg.parentElement.style.display = "block";
    $(".closebtn").removeClass("btn-hide");
}

$('#carouselExample').on('slide.bs.carousel', function (e) {

    var $e = $(e.relatedTarget);
    var idx = $e.index();
    var itemsPerSlide = 4;
    var totalItems = $('.carousel-item').length;

    if (idx >= totalItems - (itemsPerSlide - 1)) {
        var it = itemsPerSlide - (totalItems - idx);
        for (var i = 0; i < it; i++) {
            // append slides to end
            if (e.direction == "left") {
                $('.carousel-item').eq(i).appendTo('.carousel-inner');
            }
            else {
                $('.carousel-item').eq(0).appendTo('.carousel-inner');
            }
        }
    }
});

(function ($) {
    $('.dropdown-menu a.dropdown-toggle').on('click', function (e) {
        if (!$(this).next().hasClass('show')) {
            $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
        }
        var $subMenu = $(this).next(".dropdown-menu");
        $subMenu.toggleClass('show');

        $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function (e) {
            $('.dropdown-submenu .show').removeClass("show");
        });

        return false;
    });
})(jQuery)

$(document).ready(function () {

    $(".input-contact").focus(function () {
        $(this).addClass("has-value");
    });
    $(".input-contact").blur(function () {
        if ($(this).val() == "" || $(this).val() == null) $(this).removeClass("has-value");
    });
    $(".last-ads").children().first().addClass("active");
    $(".slider-home").children().first().addClass("active");
    
    /****Contact US */
    
    $(".ktc-default-section .form-field").not(":last").append('<div class="line"></div>');
    $(".ktc-default-section .form-field").css('position', 'relative');
    $(".ktc-default-section").addClass("form-row");
    $("form").addClass("block mbr-form promo mt-4");
    $('.contact-form input[type="submit"]').addClass("btn btn-success btn-send ml-1");
    $(".ktc-default-section .form-field").not(":first").not(":nth-child(2)").addClass("form-group md-form col-sm-12").removeClass("form-field");
    $(".ktc-default-section .form-field:first").addClass("form-group md-form col-sm-6").removeClass("form-field");
    $(".ktc-default-section .form-field:nth-child(2)").addClass("form-group md-form col-sm-6").removeClass("form-field");
    $(".blog-element iframe:first").css('height', '490px');
    
    $('.blog-element').bind("DOMSubtreeModified", function () {
        $(".ktc-default-section .form-field").not(":last").each(function () {
            console.log(1);
            if ($(this).find('.line').length == 0)
                $(this).append('<div class="line"></div>');
        });
    
        $(".ktc-default-section .form-field input").attr('required', 'required');
    
        $(".ktc-default-section .form-field").css('position', 'relative');
        $(".ktc-default-section").addClass("form-row");
        $("form").addClass("block mbr-form promo mt-4");
        $('input[type="submit"]').addClass("btn btn-success btn-send ml-1");
        $(".ktc-default-section .form-field").not(":first").not(":nth-child(2)").addClass("form-group md-form col-sm-12").removeClass("form-field");
        $(".ktc-default-section .form-field:first").addClass("form-group md-form col-sm-6").removeClass("form-field");
        $(".ktc-default-section .form-field:nth-child(2)").addClass("form-group md-form col-sm-6").removeClass("form-field");
        $(".blog-element iframe:first").css('height', '490px');
    });
    
    /* End Contact us */
    
    var height = 0;
    $("section.multiple-elements .item .item-content").each(function () {
        var eltHeight = $(this).height();
        if (eltHeight > height) {
            height = eltHeight;
        }
    });
    $("section.multiple-elements .item .item-content").height(height);
    
    $(".press-elements .select2-container--default .select2-search--inline .select2-search__field").attr("readonly", "readonly");
    $("#menu-button").click(function () {
        var navItem = $("#navigation").is(":visible");
        if (navItem) {
            $("#navigation").hide();
        } else {
            $("#navigation").show();
        }
    })

});
