using System.Globalization;
using System.Web.Mvc;
using System.Web.Routing;

using Kentico.Web.Mvc;
using ArchivesDuMaroc.Config;
using ArchivesDuMaroc.Infrastructure;
using ArchivesDuMaroc.Utils;

namespace ArchivesDuMaroc
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            var defaultCulture = CultureInfo.GetCultureInfo("ar-MA");
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            // Maps routes to Kentico HTTP handlers and features enabled in ApplicationConfig.cs
            // Always map the Kentico routes before adding other routes. Issues may occur if Kentico URLs are matched by a general route, for example images might not be displayed on pages
            routes.Kentico().MapRoutes();

            var route = routes.MapRoute(
                name: "Default",
                url: "{culture}/{controller}/{action}/{id}",
                defaults: new { culture = defaultCulture.Name, controller = "Accueil", action = "Index", id = UrlParameter.Optional },
                constraints: new
                {
                    culture = new SiteCultureConstraint(AppConfig.Sitename)
                }
            );

            route.RouteHandler = new MultiCultureMvcRouteHandler();
        }
    }
}
