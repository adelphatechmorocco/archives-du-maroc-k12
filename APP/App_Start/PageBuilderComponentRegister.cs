﻿using ArchivesDuMaroc.Controllers.Widgets;
using ArchivesDuMaroc.Controllers.Sections;
using Kentico.PageBuilder.Web.Mvc;


[assembly: RegisterWidget("ArchivesDuMaroc.Widget.TextWidget", typeof(TextWidgetController), "Text", IconClass = "icon-l-text")]
[assembly: RegisterWidget("ArchivesDuMaroc.Widget.ToolboxWidget", typeof(ToolboxWidgetController), "Toolbox", IconClass = "icon-tool")]
[assembly: RegisterWidget("ArchivesDuMaroc.Widget.ImageWidget", typeof(ImageWidgetController), "Image", IconClass = "icon-picture")]
[assembly: RegisterWidget("ArchivesDuMaroc.Widget.CardWidget", typeof(CardWidgetController), "Card", IconClass = "icon-ribbon")]
[assembly: RegisterWidget("ArchivesDuMaroc.Widget.ReviewWidget", typeof(ReviewWidgetController), "Press Review", IconClass = "icon-ribbon")]
[assembly: RegisterWidget("ArchivesDuMaroc.Widget.ContactWidget", typeof(ContactWidgetController), "Contact", IconClass = "icon-ribbon")]
[assembly: RegisterWidget("ArchivesDuMaroc.Widget.TrainingWidget", typeof(TrainingWidgetController), "Training", IconClass = "icon-ribbon")]


[assembly: RegisterSection("AM.SingleColumnSection", typeof(SingleColumnSectionController), "Single Column", IconClass = "icon-square")]
[assembly: RegisterSection("FourColumnSection", typeof(FourColumnSectionController), "Four Column", IconClass = "icon-square")]
[assembly: RegisterSection("TwoColumnHomeSection", typeof(TwoColumnHomeSectionController), "Two Column", IconClass = "icon-square")]