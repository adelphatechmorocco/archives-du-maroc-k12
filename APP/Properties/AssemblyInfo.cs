using System.Reflection;
using System.Runtime.InteropServices;

using CMS;

[assembly: AssemblyDiscoverable]

[assembly: AssemblyTitle("ArchivesDuMaroc")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]
[assembly: Guid("9fd10a60-2499-4bd4-bc83-73ec57a8a824")]

[assembly: AssemblyProduct("ArchivesDuMaroc")]
[assembly: AssemblyCopyright("© 2019")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyVersion("12.0.0.0")]
[assembly: AssemblyFileVersion("12.0.6900.17470")]
[assembly: AssemblyInformationalVersion("12.0.0")]
