﻿using System.Web.Mvc;
using System.Linq;
using System.Collections.Generic;

using CMS.DocumentEngine;

using Kentico.Forms.Web.Mvc;
using ArchivesDuMaroc.Models.FormComponents;

//using ArchivesDuMaroc.FormBuilder.FormComponents;
//using ArchivesDuMaroc.FormBuilder.FormComponentProperties;

[assembly: RegisterFormComponent(IconDropDownComponent.IDENTIFIER, typeof(IconDropDownComponent), "Drop-down with custom data", IconClass = "icon-menu")]
namespace ArchivesDuMaroc.Models.FormComponents
{
    public class IconDropDownComponent : SelectorFormComponent<IconDropDownComponentProperties>
    {
        public const string IDENTIFIER = "IconDropDownComponent";

        protected override IEnumerable<SelectListItem> GetItems()
        {            
            var sampleData = new string[] { "user", "check", "handshake-o", "envelope" };
            // Iterates over retrieved data and transforms it into SelectListItems
            foreach (var item in sampleData)
            {
                var listItem = new SelectListItem()
                {
                    Value = "fa fa-" + item,
                    Text = item
                };

                yield return listItem;
            }
        }
    }
}