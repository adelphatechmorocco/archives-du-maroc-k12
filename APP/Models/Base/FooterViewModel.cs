﻿using ArchivesDuMaroc.Models.Menu;
using CMS.DocumentEngine.Types.AM;
using System.Collections.Generic;

namespace ArchivesDuMaroc.Models.Base
{
    public class FooterViewModel
    {
        public Master master { get; set; }
        public List<OpeningHours> openingHours { get; set; }
        public List<SocialMedia> socialMedias { get; set; }
        public List<MenuItemViewModel> MenuItems { get; set; }
    }
}