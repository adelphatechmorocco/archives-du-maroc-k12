﻿using System;
using Kentico.PageBuilder.Web.Mvc;

namespace ArchivesDuMaroc.Models.Widgets.ImageWidget
{
    public class ImageWidgetProperties : IWidgetProperties
    {
        public Guid ImageGuid { get; set; }
    }
}