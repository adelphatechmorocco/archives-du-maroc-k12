﻿using CMS.DocumentEngine;

namespace ArchivesDuMaroc.Models.Widgets.ImageWidget
{
    public class ImageWidgetViewModel
    {
        public DocumentAttachment Image { get; set; }
    }
}