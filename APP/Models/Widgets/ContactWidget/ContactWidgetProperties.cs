﻿using Kentico.PageBuilder.Web.Mvc;

namespace ArchivesDuMaroc.Models.Widgets.ContactWidget
{
    public class ContactWidgetProperties : IWidgetProperties
    {

        public string Title { get; set; }

        public string Description { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }

        public string Text { get; set; }

    }
}