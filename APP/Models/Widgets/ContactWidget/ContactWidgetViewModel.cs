﻿namespace ArchivesDuMaroc.Models.Widgets.ContactWidget
{
    public class ContactWidgetViewModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Text { get; set; }
    }
}