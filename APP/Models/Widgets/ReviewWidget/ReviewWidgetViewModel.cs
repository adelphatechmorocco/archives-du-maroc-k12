﻿using CMS.DocumentEngine;

namespace ArchivesDuMaroc.Models.Widgets.ReviewWidget
{
    public class ReviewWidgetViewModel
    {
        public DocumentAttachment Image { get; set; }

        public string Category { get; set; }
        public string Color { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string CTALink { get; set; }
        public bool CTATarget { get; set; }
    }
}