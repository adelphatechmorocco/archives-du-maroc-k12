﻿

namespace ArchivesDuMaroc.Models.Widgets.ToolboxWidget
{
    public class ToolboxWidgetViewModel
    {
        public string Icon { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string CTALink { get; set; }
    }
}