﻿namespace ArchivesDuMaroc.Models.Widgets.TextWidget
{
    public class TextWidgetViewModel
    {
        public string Text { get; set; }
    }
}