﻿using Kentico.PageBuilder.Web.Mvc;

namespace ArchivesDuMaroc.Models.Widgets.TextWidget
{
    public class TextWidgetProperties : IWidgetProperties
    {
        public string Text { get; set; }
    }
}