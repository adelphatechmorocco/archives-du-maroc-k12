﻿using CMS.DocumentEngine;

namespace ArchivesDuMaroc.Models.Widgets.TrainingWidget
{
    public class TrainingWidgetViewModel
    {
        public DocumentAttachment Image { get; set; }

        public string Title { get; set; }
        public string Description { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Mission { get; set; }
    }
}