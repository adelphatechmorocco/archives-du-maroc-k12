﻿using System;
using Kentico.PageBuilder.Web.Mvc;

namespace ArchivesDuMaroc.Models.Widgets.TrainingWidget
{
    public class TrainingWidgetProperties : IWidgetProperties
    {
        public Guid ImageGuid { get; set; } 

        public string Title { get; set; }

        public string Description { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }

        public string Mission { get; set; }
    }
}