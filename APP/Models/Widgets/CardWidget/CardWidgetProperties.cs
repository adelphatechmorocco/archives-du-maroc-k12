﻿using System;
using CMS.SiteProvider;
using Kentico.Forms.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc;

namespace ArchivesDuMaroc.Models.Widgets.CardWidget
{
    public class CardWidgetProperties : IWidgetProperties
    {
        public Guid ImageGuid { get; set; }

        public string Category { get; set; }
        
        [EditingComponent(TextInputComponent.IDENTIFIER, Order = 0, Label = "Category color")]
        public string Color { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        private string mLinkUrl;

        [EditingComponent(TextInputComponent.IDENTIFIER, Order = 1, Label = "CTA Link")]
        [EditingComponentProperty(nameof(TextInputProperties.Placeholder), "https://www.example.com")]
        public string CTALink
        {
            get { return mLinkUrl; }
            set { mLinkUrl = GetNormalizedUrl(value, SiteContext.CurrentSite); }
        }

        private string GetNormalizedUrl(string url, SiteInfo site)
        {
            if (String.IsNullOrEmpty(url))
            {
                return url;
            }
            if (!site.SiteIsContentOnly || String.IsNullOrEmpty(site.SitePresentationURL) || !url.StartsWith(site.SitePresentationURL, StringComparison.OrdinalIgnoreCase) || !Uri.TryCreate(url, UriKind.Absolute, out _))
            {
                return url;
            }
            return $"~{url.Substring(site.SitePresentationURL.Length)}";
        }

        [EditingComponent(CheckBoxComponent.IDENTIFIER, Order = 3, Label = "Open in new window")]
        public bool CTATarget { get; set; } = false;
    }
}