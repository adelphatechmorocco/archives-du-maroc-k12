﻿using CMS.DocumentEngine;

namespace ArchivesDuMaroc.Models.Menu
{
    public class MenuItemViewModel
    {
        public string MenuItemText { get; set; }
        public string MenuItemRelativeUrl { get; set; }
        public TreeNodeCollection Children { get; set; }
    }
}