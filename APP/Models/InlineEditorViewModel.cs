﻿namespace ArchivesDuMaroc.Models
{
    public abstract class InlineEditorViewModel
    {
        public string PropertyName { get; set; }
    }
}