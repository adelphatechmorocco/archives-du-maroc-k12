﻿using CMS.DocumentEngine.Types.AM;
using System;

namespace ArchivesDuMaroc.Models
{
    public class ConsultationRoomViewModel
    {
        public string DocumentName { get; }
        public string ConsultationRoomTitle { get; }
        public string ConsultationRoomHeroImage { get; }
        public string ConsultationRoomImage { get; }
        public string ConsultationRoomText { get; }
        public string ConsultationRoomQuote { get; }
        public string FileRegulation { get; }
        public string FileSubscription { get; }
        public string TitleRegulation { get; }
        public string TitleSubscription { get; }
        public Guid NodeGUID { get; set; }

        public ConsultationRoomViewModel(ConsultationRoom ConsultationRoomPage)
        {
            DocumentName = ConsultationRoomPage.DocumentName;
            ConsultationRoomTitle = ConsultationRoomPage.ConsultationRoomTitle;
            ConsultationRoomHeroImage = ConsultationRoomPage.ConsultationRoomHeroImage;
            ConsultationRoomImage = ConsultationRoomPage.ConsultationRoomImage;
            ConsultationRoomText = ConsultationRoomPage.ConsultationRoomText;
            ConsultationRoomQuote = ConsultationRoomPage.ConsultationRoomQuote;
            FileRegulation = ConsultationRoomPage.FileRegulation;
            FileSubscription = ConsultationRoomPage.FileSubscription;
            TitleRegulation = ConsultationRoomPage.TitleRegulation;
            TitleSubscription = ConsultationRoomPage.TitleSubscription;
            NodeGUID = ConsultationRoomPage.NodeGUID;
        }
    }
}