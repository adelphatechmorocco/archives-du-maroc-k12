﻿using CMS.DocumentEngine.Types.AM;
using System;
using System.Collections.Generic;

namespace ArchivesDuMaroc.Models
{
    public class RegulationsViewModel
    {
        public string DocumentName { get; }
        public string RegulationsTitle { get; }
        public string RegulationsHeroImage { get; }
        public string RegulationsText { get; }

        public List<RegulationsFile> Slider { get; }
        public Guid NodeGUID { get; set; }

        public RegulationsViewModel(Regulations RegulationsPage, List<RegulationsFile> slider)
        {
            DocumentName = RegulationsPage.DocumentName;
            RegulationsTitle = RegulationsPage.RegulationsTitle;
            RegulationsHeroImage = RegulationsPage.RegulationsHeroImage;
            RegulationsText = RegulationsPage.RegulationsText;
            Slider = slider;
            NodeGUID = RegulationsPage.NodeGUID;
        }
    }
}