﻿using CMS.DocumentEngine.Types.AM;
using System.Collections.Generic;

namespace ArchivesDuMaroc.Models
{
    public class RegulationViewModel
    {
        public string DocumentName { get; }
        public string RegulationTitle { get; }
        public string RegulationHeroImage { get; }
        public string RegulationText { get; }

        public List<RegulationsFile> Slider { get; }

        // Maps the data from the Home page type's fields to the view model properties
        public RegulationViewModel(Regulations RegulationPage, List<RegulationsFile> slider)
        {
            DocumentName = RegulationPage.DocumentName;
            RegulationTitle = RegulationPage.RegulationTitle;
            RegulationHeroImage = RegulationPage.RegulationHeroImage;
            RegulationText = RegulationPage.RegulationText;
            Slider = slider;
        }
    }
}