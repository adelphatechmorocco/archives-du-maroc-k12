﻿using CMS.DocumentEngine.Types.AM;
using System;
using System.Collections.Generic;

namespace ArchivesDuMaroc.Models
{
    public class PageOpeningHoursViewModel
    {
        public string DocumentName { get; }
        public string PageOpeningHoursTitle { get; }
        public string PageOpeningHoursHeroImage { get; }
        public string PageOpeningHoursText { get; }

        public List<OpeningHoursItems> Slider { get; }
        public Guid NodeGUID { get; set; }

        public PageOpeningHoursViewModel(PageOpeningHours PageOpeningHoursPage, List<OpeningHoursItems> Slider)
        {
            DocumentName = PageOpeningHoursPage.DocumentName;
            PageOpeningHoursTitle = PageOpeningHoursPage.PageOpeningHoursTitle;
            PageOpeningHoursHeroImage = PageOpeningHoursPage.PageOpeningHoursHeroImage;
            PageOpeningHoursText = PageOpeningHoursPage.PageOpeningHoursText;
            this.Slider = Slider;
            NodeGUID = PageOpeningHoursPage.NodeGUID;
        }
    }
}