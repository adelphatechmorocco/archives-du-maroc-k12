﻿using CMS.DocumentEngine.Types.AM;
using System.Collections.Generic;
using System;

namespace ArchivesDuMaroc.Models
{
    public class NoticeViewModel
    {
        public string DocumentName { get; }
        public string NoticeTitle { get; }
        public string NoticeHeroImage { get; }
        public Guid NodeGUID { get; set; }

        public List<NoticeItem> Slider { get; }
        
        public NoticeViewModel(Notice NoticePage, List<NoticeItem> slider)
        {
            DocumentName = NoticePage.DocumentName;
            NoticeTitle = NoticePage.NoticeHeroTitle;
            NoticeHeroImage = NoticePage.NoticeHeroImage;
            Slider = slider;
            NodeGUID = NoticePage.NodeGUID;
        }
    }
}