﻿using CMS.DocumentEngine.Types.AM;
using System.Collections.Generic;

namespace ArchivesDuMaroc.Models
{
    public class NoticeViewModel
    {
        public string DocumentName { get; }
        public string NoticeTitle { get; }
        public string NoticeHeroImage { get; }

        public List<NoticeItem> Slider { get; }

        // Maps the data from the Home page type's fields to the view model properties
        public NoticeViewModel(Notice NoticePage, List<NoticeItem> slider)
        {
            DocumentName = NoticePage.DocumentName;
            NoticeTitle = NoticePage.NoticeHeroTitle;
            NoticeHeroImage = NoticePage.NoticeHeroImage;
            Slider = slider;
        }
    }
}