﻿using CMS.DocumentEngine.Types.AM;
using System;
using System.Collections.Generic;

namespace ArchivesDuMaroc.Models
{
    public class ArchivalToolsViewModel
    {
        public string DocumentName { get; }
        public string ArchivalToolsTitle { get; }
        public string ArchivalToolsHeroImage { get; }
        public string ArchivalToolsText { get; }

        public List<ArchivalToolsFiles> Slider { get; }
        public Guid NodeGUID { get; set; }

        public ArchivalToolsViewModel(ArchivalTools ArchivalToolsPage, List<ArchivalToolsFiles> slider)
        {
            DocumentName = ArchivalToolsPage.DocumentName;
            ArchivalToolsTitle = ArchivalToolsPage.ArchivalToolsTitle;
            ArchivalToolsHeroImage = ArchivalToolsPage.ArchivalToolsHeroImage;
            ArchivalToolsText = ArchivalToolsPage.ArchivalToolsText;
            NodeGUID = ArchivalToolsPage.NodeGUID;
            Slider = slider;
        }
    }
}