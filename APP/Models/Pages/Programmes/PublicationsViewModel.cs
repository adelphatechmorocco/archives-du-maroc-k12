﻿using CMS.DocumentEngine.Types.AM;
using System;
using System.Collections.Generic;

namespace ArchivesDuMaroc.Models
{
    public class ForecastProgramViewModel
    {
        public string DocumentName { get; }
        public string ForecastProgramHeroTitle { get; }
        public string ForecastProgramHeroImage { get; }
        public Guid NodeGUID { get; set; }
        public List<ForecastItem> Slider { get; }
        
        public ForecastProgramViewModel(ForecastProgram ForecastProgramPage, List<ForecastItem> slider)
        {
            DocumentName = ForecastProgramPage.DocumentName;
            ForecastProgramHeroTitle = ForecastProgramPage.ForecastProgramHeroTitle;
            ForecastProgramHeroImage = ForecastProgramPage.ForecastProgramHeroImage;
            Slider = slider;
            NodeGUID = ForecastProgramPage.NodeGUID;
        }
    }
}