﻿using CMS.DocumentEngine.Types.AM;
using System;
using System.Collections.Generic;

namespace ArchivesDuMaroc.Models
{
    public class NewspapersViewModel
    {
        public string DocumentName { get; }
        public string NewspapersHeroTitle { get; }
        public string NewspapersHeroImage { get; }
        public Guid NodeGUID { get; set; }

        public List<NewspaperItem> Slider { get; }
        public List<int> Years { get; }
        public List<int> SelectedYears { get; }
        
        public NewspapersViewModel(Newspapers NewspapersPage, List<NewspaperItem> slider, List<int> years, List<int> selectedYears)
        {
            DocumentName = NewspapersPage.DocumentName;
            NewspapersHeroTitle = NewspapersPage.NewspapersHeroTitle;
            NewspapersHeroImage = NewspapersPage.NewspapersHeroImage;
            NodeGUID = NewspapersPage.NodeGUID;
            Slider = slider;
            Years = years;
            SelectedYears = selectedYears;
        }
    }
}