﻿using CMS.DocumentEngine.Types.AM;
using System;

namespace ArchivesDuMaroc.Models
{
    public class NewspaperItemViewModel
    {
        public string DocumentName { get; }
        public string NewspaperTitle { get; }
        public string NewspaperHeroImage { get; }
        public string NewspaperImage { get; }
        public string NewspaperYear { get; }
        public string NewspaperSource { get; }
        public string NewspaperFile { get; }
        public string NewspaperDescription { get; }
        public Guid NodeGUID { get; set; }

        public NewspaperItemViewModel(NewspaperItem NewspaperPage, string HeroImage)
        {
            DocumentName = NewspaperPage.DocumentName;
            NewspaperTitle = NewspaperPage.NewspaperTitle;
            NewspaperHeroImage = HeroImage;
            NewspaperImage = NewspaperPage.NewspaperImage;
            NewspaperYear = NewspaperPage.NewspaperYear;
            NewspaperSource = NewspaperPage.NewspaperSource;
            NewspaperFile = NewspaperPage.NewspaperFile;
            NewspaperDescription = NewspaperPage.NewspaperDescription;
            NodeGUID = NewspaperPage.Parent.NodeGUID;
        }
    }
}