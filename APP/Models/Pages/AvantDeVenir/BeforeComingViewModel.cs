﻿using CMS.DocumentEngine.Types.AM;
using System;

namespace ArchivesDuMaroc.Models
{
    public class BeforeComingViewModel
    {
        public string DocumentName { get; }
        public string BeforeComingTitle { get; }
        public string BeforeComingHeroImage { get; }
        public string BeforeComingText { get; }
        public Guid NodeGUID { get; set; }

        // Maps the data from the Home page type's fields to the view model properties
        public BeforeComingViewModel(BeforeComing BeforeComingPage)
        {
            DocumentName = BeforeComingPage.DocumentName;
            BeforeComingTitle = BeforeComingPage.BeforeComingTitle;
            BeforeComingHeroImage = BeforeComingPage.BeforeComingHeroImage;
            BeforeComingText = BeforeComingPage.BeforeComingText;
            NodeGUID = BeforeComingPage.NodeGUID;
        }
    }
}