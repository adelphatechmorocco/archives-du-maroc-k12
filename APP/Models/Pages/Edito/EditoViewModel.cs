﻿using CMS.DocumentEngine.Types.AM;
using System;

namespace ArchivesDuMaroc.Models
{
    public class EditoViewModel
    {
        public string DocumentName { get; }
        public string EditoTitle { get; }
        public string EditoHeroImage { get; }
        public string EditoImage { get; }
        public string EditoText { get; }
        public string EditoQuote { get; }
        public Guid NodeGUID{get; set;}

        public EditoViewModel(Edito homePage)
        {
            DocumentName = homePage.DocumentName;
            EditoTitle = homePage.EditoTitle;
            EditoHeroImage = homePage.EditoHeroImage;
            EditoImage = homePage.EditoImage;
            EditoText = homePage.EditoText;
            EditoQuote = homePage.EditoQuote;
            NodeGUID = homePage.NodeGUID;
        }
    }
}