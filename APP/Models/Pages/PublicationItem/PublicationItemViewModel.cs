﻿using CMS.DocumentEngine.Types.AM;

namespace ArchivesDuMaroc.Models
{
    public class PublicationItemViewModel
    {
        public string DocumentName { get; }
        public string PublicationTitle { get; }
        public string PublicationHeroImage { get; }
        public string PublicationImage { get; }
        public string PublicationText { get; }
        public string PublicationReferences { get; }
        public string PublicationFile { get; }
        public string PublicationRelativeURL { get; }


        public PublicationItemViewModel(PublicationItems PublicationPage)
        {
            DocumentName = PublicationPage.DocumentName;
            PublicationTitle = PublicationPage.PublicationTitle;
            PublicationHeroImage = PublicationPage.PublicationHeroImage;
            PublicationImage = PublicationPage.PublicationImage;
            PublicationText = PublicationPage.PublicationText;
            PublicationReferences = PublicationPage.PublicationReferences;
            PublicationFile = PublicationPage.PublicationFile;
            PublicationRelativeURL = PublicationPage.RelativeURL;
        }
    }
}