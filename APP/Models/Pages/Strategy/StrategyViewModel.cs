﻿using CMS.DocumentEngine.Types.AM;
using System;

namespace ArchivesDuMaroc.Models
{
    public class StrategyViewModel
    {
        public string DocumentName { get; }
        public string StrategyTitle { get; }
        public string StrategyHeroImage { get; }
        public string StrategyText { get; }
        public Guid NodeGUID { get; set; }

        public StrategyViewModel(Strategy StrategyPage)
        {
            DocumentName = StrategyPage.DocumentName;
            StrategyTitle = StrategyPage.StrategyTitle;
            StrategyHeroImage = StrategyPage.StrategyHeroImage;
            StrategyText = StrategyPage.StrategyText;
            NodeGUID = StrategyPage.NodeGUID;
        }
    }
}