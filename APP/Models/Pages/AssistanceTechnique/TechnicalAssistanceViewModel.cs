﻿using CMS.DocumentEngine.Types.AM;
using System;

namespace ArchivesDuMaroc.Models
{
    public class TechnicalAssistanceViewModel
    {
        public string DocumentName { get; }
        public string TechnicalAssistanceTitle { get; }
        public string TechnicalAssistanceHeroImage { get; }
        public Guid NodeGUID { get; set; }

        // Maps the data from the Home page type's fields to the view model properties
        public TechnicalAssistanceViewModel(TechnicalAssistance TechnicalAssistancePage)
        {
            DocumentName = TechnicalAssistancePage.DocumentName;
            TechnicalAssistanceTitle = TechnicalAssistancePage.TechnicalAssistanceTitle;
            TechnicalAssistanceHeroImage = TechnicalAssistancePage.TechnicalAssistanceHeroImage;
            NodeGUID = TechnicalAssistancePage.NodeGUID;
    }
    }
}