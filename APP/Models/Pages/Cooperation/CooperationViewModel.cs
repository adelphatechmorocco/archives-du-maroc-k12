﻿using CMS.DocumentEngine.Types.AM;
using System;
using System.Collections.Generic;

namespace ArchivesDuMaroc.Models
{
    public class CooperationViewModel
    {
        public string DocumentName { get; }
        public string CooperationTitle { get; }
        public string CooperationHeroImage { get; }
        public string CooperationText { get; }
        public Guid NodeGUID { get; set; }

        public List<SliderCooperation> Slider { get; }

        // Maps the data from the Home page type's fields to the view model properties
        public CooperationViewModel(Cooperation CooperationPage, List<SliderCooperation> slider)
        {
            DocumentName = CooperationPage.DocumentName;
            CooperationTitle = CooperationPage.CooperationTitle;
            CooperationHeroImage = CooperationPage.CooperationHeroImage;
            CooperationText = CooperationPage.CooperationText;
            NodeGUID = CooperationPage.NodeGUID;
            Slider = slider;
        }
    }
}