﻿using CMS.DocumentEngine.Types.AM;
using System;
using System.Collections.Generic;

namespace ArchivesDuMaroc.Models
{
    public class TendersViewModel
    {
        public string DocumentName { get; }
        public string TendersHeroTitle { get; }
        public string TendersHeroImage { get; }
        public Guid NodeGUID { get; set; }

        public List<TendersItemViewModel> Slider { get; }

        public TendersViewModel(Tenders TendersPage, List<TendersItemViewModel> Slider)
        {
            DocumentName = TendersPage.DocumentName;
            TendersHeroTitle = TendersPage.TendersHeroTitle;
            TendersHeroImage = TendersPage.TendersHeroImage;
            NodeGUID = TendersPage.NodeGUID;
            this.Slider = Slider;
        }
    }
}