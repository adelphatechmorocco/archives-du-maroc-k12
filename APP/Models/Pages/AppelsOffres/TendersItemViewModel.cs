﻿using CMS.DocumentEngine.Types.AM;
using System.Collections.Generic;

namespace ArchivesDuMaroc.Models
{
    public class TendersItemViewModel
    {
        public string DocumentName { get; }
        public string TendersItemTitle { get; }
        public string TendersItemText { get; }

        public List<TendersFiles> Slider { get; }

        public TendersItemViewModel(TendersItem TendersItemPage, List<TendersFiles> Slider)
        {
            DocumentName = TendersItemPage.DocumentName;
            TendersItemTitle = TendersItemPage.TendersItemTitle;
            TendersItemText = TendersItemPage.TendersItemText;
            this.Slider = Slider;
        }

        public TendersItemViewModel()
        {
        }
    }
}