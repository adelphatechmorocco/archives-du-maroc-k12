﻿using CMS.DocumentEngine.Types.AM;
using System;

namespace ArchivesDuMaroc.Models
{
    public class ProfessionalTrainingViewModel
    {
        public string DocumentName { get; }
        public string ProfessionalTrainingTitle { get; }
        public string ProfessionalTrainingHeroImage { get; }
        public Guid NodeGUID { get; set; }

        public ProfessionalTrainingViewModel(ProfessionalTraining ProfessionalTrainingPage)
        {
            DocumentName = ProfessionalTrainingPage.DocumentName;
            ProfessionalTrainingTitle = ProfessionalTrainingPage.ProfessionalTrainingTitle;
            ProfessionalTrainingHeroImage = ProfessionalTrainingPage.ProfessionalTrainingHeroImage;
            NodeGUID = ProfessionalTrainingPage.NodeGUID;
        }
    }
}