﻿using CMS.DocumentEngine.Types.AM;
using System;

namespace ArchivesDuMaroc.Models
{
    public class EntrustArchivesViewModel
    {
        public string DocumentName { get; }
        public string EntrustArchivesTitle { get; }
        public string EntrustArchivesHeroImage { get; }
        public string EntrustArchivesImage { get; }
        public string EntrustArchivesText { get; }
        public string EntrustArchivesQuote { get; }
        public Guid NodeGUID { get; set; }

        // Maps the data from the Home page type's fields to the view model properties
        public EntrustArchivesViewModel(EntrustArchives EntrustArchivesPage)
        {
            DocumentName = EntrustArchivesPage.DocumentName;
            EntrustArchivesTitle = EntrustArchivesPage.EntrustArchivesTitle;
            EntrustArchivesHeroImage = EntrustArchivesPage.EntrustArchivesHeroImage;
            EntrustArchivesImage = EntrustArchivesPage.EntrustArchivesImage;
            EntrustArchivesText = EntrustArchivesPage.EntrustArchivesText;
            EntrustArchivesQuote = EntrustArchivesPage.EntrustArchivesQuote;
            NodeGUID = EntrustArchivesPage.NodeGUID;
        }
    }
}