﻿using CMS.DocumentEngine.Types.AM;
using System;

namespace ArchivesDuMaroc.Models
{
    public class ContactUsViewModel
    {
        public string DocumentName { get; }
        public string ContactUsTitle { get; }
        public string ContactUsHeroImage { get; }
        public string ContactUsImage { get; }
        public string ContactUsImageTitle { get; }
        public string ContactUsText { get; }
        public string ContactUsLocation { get; }
        public Guid NodeGUID { get; set; }

        // Maps the data from the Home page type's fields to the view model properties
        public ContactUsViewModel(ContactUs ContactUsPage, string location)
        {
            DocumentName = ContactUsPage.DocumentName;
            ContactUsTitle = ContactUsPage.ContactUsTitle;
            ContactUsHeroImage = ContactUsPage.ContactUsHeroImage;
            ContactUsLocation = location;
            NodeGUID = ContactUsPage.NodeGUID;
        }
    }
}