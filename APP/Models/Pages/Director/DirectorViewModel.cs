﻿using CMS.DocumentEngine.Types.AM;
using System;

namespace ArchivesDuMaroc.Models
{
    public class DirectorViewModel
    {
        public string DocumentName { get; }
        public string DirectorTitle { get; }
        public string DirectorHeroImage { get; }
        public string DirectorImage { get; }
        public string DirectorImageTitle { get; }
        public string DirectorText { get; }
        public Guid NodeGUID { get; set; }

        public DirectorViewModel(Director directorPage)
        {
            DocumentName = directorPage.DocumentName;
            DirectorTitle = directorPage.DirectorTitle;
            DirectorHeroImage = directorPage.DirectorHeroImage;
            DirectorImage = directorPage.DirectorImage;
            DirectorImageTitle = directorPage.DirectorImageTitle;
            DirectorText = directorPage.DirectorText;
            NodeGUID = directorPage.NodeGUID;
        }
    }
}