﻿using CMS.DocumentEngine.Types.AM;
using System;
using System.Collections.Generic;

namespace ArchivesDuMaroc.Models
{
    public class ActivityViewModel
    {
        public string DocumentName { get; }
        public string RelativeURL { get; }
        public string ActivityTitle { get; }
        public string ActivityHeroImage { get; }
        public string ActivityText { get; }
        public string ActivityMainTitle { get; }
        public string ActivitySubTitle { get; }
        public string ActivityMainImage { get; }
        public DateTime ActivityDateStart { get; }
        public DateTime ActivityDatend { get; }
        public string ActivityTime { get; }
        public string ActivityLocation { get; }
        public string[] ActivityTags { get; }
        public Guid NodeGUID { get; }

        public List<ActivityImages> SliderImages { get; }
        public List<ActivityFiles> SliderFiles { get; }
        
        public ActivityViewModel(Activity ActivityPage, List<ActivityFiles> SliderFiles, List<ActivityImages> SliderImages)
        {
            DocumentName = ActivityPage.DocumentName;
            RelativeURL = ActivityPage.RelativeURL;
            ActivityTitle = ActivityPage.ActivityTitle;
            ActivityHeroImage = ActivityPage.ActivityHeroImage;
            ActivityText = ActivityPage.ActivityText;
            ActivityMainTitle = ActivityPage.ActivityMainTitle;
            ActivitySubTitle = ActivityPage.ActivitySubTitle;
            ActivityMainImage = ActivityPage.ActivityMainImage;
            ActivityDateStart = ActivityPage.ActivityDateStart;
            ActivityDatend = ActivityPage.ActivityDatend;
            ActivityTime = ActivityPage.ActivityTime;
            ActivityLocation = ActivityPage.ActivityLocation;
            ActivityTags = ActivityPage.ActivityTags.Split(',');
            NodeGUID = ActivityPage.NodeGUID;
            this.SliderImages = SliderImages;
            this.SliderFiles = SliderFiles;
        }
    }
}