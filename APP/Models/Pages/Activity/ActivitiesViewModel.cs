﻿using CMS.DocumentEngine.Types.AM;
using System.Collections.Generic;
using System;

namespace ArchivesDuMaroc.Models
{
    public class ActivitiesViewModel
    {
        public string DocumentName { get; }
        public string ActivitiesTitle { get; }
        public string ActivitiesHeroImage { get; }
        public List<ActivityViewModel> Activities { get; }
        public Guid NodeGUID { get; set; }

        public ActivitiesViewModel(Activities activitiesPage, List<ActivityViewModel> Activities)
        {
            DocumentName = activitiesPage.DocumentName;
            ActivitiesTitle = activitiesPage.ActivitiesTitle;
            ActivitiesHeroImage = activitiesPage.ActivitiesHeroImage;
            NodeGUID = activitiesPage.NodeGUID;
            this.Activities = Activities;
        }
    }
}