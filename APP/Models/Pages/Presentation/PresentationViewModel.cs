﻿using CMS.DocumentEngine.Types.AM;
using System;

namespace ArchivesDuMaroc.Models
{
    public class PresentationViewModel
    {
        public string DocumentName { get; }
        public string PresentationTitle { get; }
        public string PresentationHeroImage { get; }
        public string PresentationImage { get; }
        public string PresentationText { get; }
        public Guid NodeGUID { get; set; }


        public PresentationViewModel(Presentation PresentationPage)
        {
            DocumentName = PresentationPage.DocumentName;
            PresentationTitle = PresentationPage.PresentationTitle;
            PresentationHeroImage = PresentationPage.PresentationHeroImage;
            PresentationImage = PresentationPage.PresentationImage;
            PresentationText = PresentationPage.PresentationText;
            NodeGUID = PresentationPage.NodeGUID;
        }
    }
}