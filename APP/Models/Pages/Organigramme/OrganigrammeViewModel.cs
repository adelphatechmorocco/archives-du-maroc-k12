﻿using CMS.DocumentEngine.Types.AM;
using System;

namespace ArchivesDuMaroc.Models
{
    public class OrganigrammeViewModel
    {
        public string DocumentName { get; }
        public string OrganigrammeTitle { get; }
        public string OrganigrammeHeroImage { get; }
        public string OrganigrammeImage { get; }
        public Guid NodeGUID { get; set; }

        public OrganigrammeViewModel(Organigramme OrganigrammePage)
        {
            DocumentName = OrganigrammePage.DocumentName;
            OrganigrammeTitle = OrganigrammePage.OrganigrammeTitle;
            OrganigrammeHeroImage = OrganigrammePage.OrganigrammeHeroImage;
            OrganigrammeImage = OrganigrammePage.OrganigrammeImage;
            NodeGUID = OrganigrammePage.NodeGUID;
        }
    }
}