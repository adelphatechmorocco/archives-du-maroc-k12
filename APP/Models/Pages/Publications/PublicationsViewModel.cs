﻿using CMS.DocumentEngine.Types.AM;
using System;
using System.Collections.Generic;

namespace ArchivesDuMaroc.Models
{
    public class PublicationsViewModel
    {
        public string DocumentName { get; }
        public string PublicationsTitle { get; }
        public string PublicationsHeroImage { get; }
        public Guid NodeGUID { get; set; }

        public List<PublicationItems> Slider { get; }

        // Maps the data from the Home page type's fields to the view model properties
        public PublicationsViewModel(Publications PublicationsPage, List<PublicationItems> slider)
        {
            DocumentName = PublicationsPage.DocumentName;
            PublicationsTitle = PublicationsPage.PublicationsTitle;
            PublicationsHeroImage = PublicationsPage.PublicationsHeroImage;
            NodeGUID = PublicationsPage.NodeGUID;
            Slider = slider;
        }
    }
}