//--------------------------------------------------------------------------------------------------
// <auto-generated>
//
//     This code was generated by code generator tool.
//
//     To customize the code use your own partial class. For more info about how to use and customize
//     the generated code see the documentation at http://docs.kentico.com.
//
// </auto-generated>
//--------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;

using CMS;
using CMS.Base;
using CMS.Helpers;
using CMS.DataEngine;
using CMS.DocumentEngine.Types.AM;
using CMS.DocumentEngine;

[assembly: RegisterDocumentType(Edito.CLASS_NAME, typeof(Edito))]

namespace CMS.DocumentEngine.Types.AM
{
	/// <summary>
	/// Represents a content item of type Edito.
	/// </summary>
	public partial class Edito : TreeNode
	{
		#region "Constants and variables"

		/// <summary>
		/// The name of the data class.
		/// </summary>
		public const string CLASS_NAME = "AM.Edito";


		/// <summary>
		/// The instance of the class that provides extended API for working with Edito fields.
		/// </summary>
		private readonly EditoFields mFields;

		#endregion


		#region "Properties"

		/// <summary>
		/// EditoID.
		/// </summary>
		[DatabaseIDField]
		public int EditoID
		{
			get
			{
				return ValidationHelper.GetInteger(GetValue("EditoID"), 0);
			}
			set
			{
				SetValue("EditoID", value);
			}
		}


		/// <summary>
		/// Edito Title.
		/// </summary>
		[DatabaseField]
		public string EditoTitle
		{
			get
			{
				return ValidationHelper.GetString(GetValue("EditoTitle"), @"");
			}
			set
			{
				SetValue("EditoTitle", value);
			}
		}


		/// <summary>
		/// Edito Hero Image.
		/// </summary>
		[DatabaseField]
		public string EditoHeroImage
		{
			get
			{
				return ValidationHelper.GetString(GetValue("EditoHeroImage"), @"");
			}
			set
			{
				SetValue("EditoHeroImage", value);
			}
		}


		/// <summary>
		/// Edito Image.
		/// </summary>
		[DatabaseField]
		public string EditoImage
		{
			get
			{
				return ValidationHelper.GetString(GetValue("EditoImage"), @"");
			}
			set
			{
				SetValue("EditoImage", value);
			}
		}


		/// <summary>
		/// Edito Text.
		/// </summary>
		[DatabaseField]
		public string EditoText
		{
			get
			{
				return ValidationHelper.GetString(GetValue("EditoText"), @"");
			}
			set
			{
				SetValue("EditoText", value);
			}
		}


		/// <summary>
		/// Edito Quote.
		/// </summary>
		[DatabaseField]
		public string EditoQuote
		{
			get
			{
				return ValidationHelper.GetString(GetValue("EditoQuote"), @"");
			}
			set
			{
				SetValue("EditoQuote", value);
			}
		}


		/// <summary>
		/// Gets an object that provides extended API for working with Edito fields.
		/// </summary>
		[RegisterProperty]
		public EditoFields Fields
		{
			get
			{
				return mFields;
			}
		}


		/// <summary>
		/// Provides extended API for working with Edito fields.
		/// </summary>
		[RegisterAllProperties]
		public partial class EditoFields : AbstractHierarchicalObject<EditoFields>
		{
			/// <summary>
			/// The content item of type Edito that is a target of the extended API.
			/// </summary>
			private readonly Edito mInstance;


			/// <summary>
			/// Initializes a new instance of the <see cref="EditoFields" /> class with the specified content item of type Edito.
			/// </summary>
			/// <param name="instance">The content item of type Edito that is a target of the extended API.</param>
			public EditoFields(Edito instance)
			{
				mInstance = instance;
			}


			/// <summary>
			/// EditoID.
			/// </summary>
			public int ID
			{
				get
				{
					return mInstance.EditoID;
				}
				set
				{
					mInstance.EditoID = value;
				}
			}


			/// <summary>
			/// Edito Title.
			/// </summary>
			public string Title
			{
				get
				{
					return mInstance.EditoTitle;
				}
				set
				{
					mInstance.EditoTitle = value;
				}
			}


			/// <summary>
			/// Edito Hero Image.
			/// </summary>
			public string HeroImage
			{
				get
				{
					return mInstance.EditoHeroImage;
				}
				set
				{
					mInstance.EditoHeroImage = value;
				}
			}


			/// <summary>
			/// Edito Image.
			/// </summary>
			public string Image
			{
				get
				{
					return mInstance.EditoImage;
				}
				set
				{
					mInstance.EditoImage = value;
				}
			}


			/// <summary>
			/// Edito Text.
			/// </summary>
			public string Text
			{
				get
				{
					return mInstance.EditoText;
				}
				set
				{
					mInstance.EditoText = value;
				}
			}


			/// <summary>
			/// Edito Quote.
			/// </summary>
			public string Quote
			{
				get
				{
					return mInstance.EditoQuote;
				}
				set
				{
					mInstance.EditoQuote = value;
				}
			}
		}

		#endregion


		#region "Constructors"

		/// <summary>
		/// Initializes a new instance of the <see cref="Edito" /> class.
		/// </summary>
		public Edito() : base(CLASS_NAME)
		{
			mFields = new EditoFields(this);
		}

		#endregion
	}
}