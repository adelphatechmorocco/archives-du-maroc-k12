//--------------------------------------------------------------------------------------------------
// <auto-generated>
//
//     This code was generated by code generator tool.
//
//     To customize the code use your own partial class. For more info about how to use and customize
//     the generated code see the documentation at http://docs.kentico.com.
//
// </auto-generated>
//--------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;

using CMS;
using CMS.Base;
using CMS.Helpers;
using CMS.DataEngine;
using CMS.DocumentEngine.Types.AM;
using CMS.DocumentEngine;

[assembly: RegisterDocumentType(Strategy.CLASS_NAME, typeof(Strategy))]

namespace CMS.DocumentEngine.Types.AM
{
	/// <summary>
	/// Represents a content item of type Strategy.
	/// </summary>
	public partial class Strategy : TreeNode
	{
		#region "Constants and variables"

		/// <summary>
		/// The name of the data class.
		/// </summary>
		public const string CLASS_NAME = "AM.Strategy";


		/// <summary>
		/// The instance of the class that provides extended API for working with Strategy fields.
		/// </summary>
		private readonly StrategyFields mFields;

		#endregion


		#region "Properties"

		/// <summary>
		/// StrategyID.
		/// </summary>
		[DatabaseIDField]
		public int StrategyID
		{
			get
			{
				return ValidationHelper.GetInteger(GetValue("StrategyID"), 0);
			}
			set
			{
				SetValue("StrategyID", value);
			}
		}


		/// <summary>
		/// Strategy Title.
		/// </summary>
		[DatabaseField]
		public string StrategyTitle
		{
			get
			{
				return ValidationHelper.GetString(GetValue("StrategyTitle"), @"");
			}
			set
			{
				SetValue("StrategyTitle", value);
			}
		}


		/// <summary>
		/// Strategy Hero Image.
		/// </summary>
		[DatabaseField]
		public string StrategyHeroImage
		{
			get
			{
				return ValidationHelper.GetString(GetValue("StrategyHeroImage"), @"");
			}
			set
			{
				SetValue("StrategyHeroImage", value);
			}
		}


		/// <summary>
		/// Strategy Text.
		/// </summary>
		[DatabaseField]
		public string StrategyText
		{
			get
			{
				return ValidationHelper.GetString(GetValue("StrategyText"), @"");
			}
			set
			{
				SetValue("StrategyText", value);
			}
		}


		/// <summary>
		/// Gets an object that provides extended API for working with Strategy fields.
		/// </summary>
		[RegisterProperty]
		public StrategyFields Fields
		{
			get
			{
				return mFields;
			}
		}


		/// <summary>
		/// Provides extended API for working with Strategy fields.
		/// </summary>
		[RegisterAllProperties]
		public partial class StrategyFields : AbstractHierarchicalObject<StrategyFields>
		{
			/// <summary>
			/// The content item of type Strategy that is a target of the extended API.
			/// </summary>
			private readonly Strategy mInstance;


			/// <summary>
			/// Initializes a new instance of the <see cref="StrategyFields" /> class with the specified content item of type Strategy.
			/// </summary>
			/// <param name="instance">The content item of type Strategy that is a target of the extended API.</param>
			public StrategyFields(Strategy instance)
			{
				mInstance = instance;
			}


			/// <summary>
			/// StrategyID.
			/// </summary>
			public int ID
			{
				get
				{
					return mInstance.StrategyID;
				}
				set
				{
					mInstance.StrategyID = value;
				}
			}


			/// <summary>
			/// Strategy Title.
			/// </summary>
			public string Title
			{
				get
				{
					return mInstance.StrategyTitle;
				}
				set
				{
					mInstance.StrategyTitle = value;
				}
			}


			/// <summary>
			/// Strategy Hero Image.
			/// </summary>
			public string HeroImage
			{
				get
				{
					return mInstance.StrategyHeroImage;
				}
				set
				{
					mInstance.StrategyHeroImage = value;
				}
			}


			/// <summary>
			/// Strategy Text.
			/// </summary>
			public string Text
			{
				get
				{
					return mInstance.StrategyText;
				}
				set
				{
					mInstance.StrategyText = value;
				}
			}
		}

		#endregion


		#region "Constructors"

		/// <summary>
		/// Initializes a new instance of the <see cref="Strategy" /> class.
		/// </summary>
		public Strategy() : base(CLASS_NAME)
		{
			mFields = new StrategyFields(this);
		}

		#endregion
	}
}