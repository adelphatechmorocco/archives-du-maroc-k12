//--------------------------------------------------------------------------------------------------
// <auto-generated>
//
//     This code was generated by code generator tool.
//
//     To customize the code use your own partial class. For more info about how to use and customize
//     the generated code see the documentation at http://docs.kentico.com.
//
// </auto-generated>
//--------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;

using CMS;
using CMS.Base;
using CMS.Helpers;
using CMS.DataEngine;
using CMS.DocumentEngine.Types.AM;
using CMS.DocumentEngine;

[assembly: RegisterDocumentType(Tenders.CLASS_NAME, typeof(Tenders))]

namespace CMS.DocumentEngine.Types.AM
{
	/// <summary>
	/// Represents a content item of type Tenders.
	/// </summary>
	public partial class Tenders : TreeNode
	{
		#region "Constants and variables"

		/// <summary>
		/// The name of the data class.
		/// </summary>
		public const string CLASS_NAME = "AM.Tenders";


		/// <summary>
		/// The instance of the class that provides extended API for working with Tenders fields.
		/// </summary>
		private readonly TendersFields mFields;

		#endregion


		#region "Properties"

		/// <summary>
		/// TendersID.
		/// </summary>
		[DatabaseIDField]
		public int TendersID
		{
			get
			{
				return ValidationHelper.GetInteger(GetValue("TendersID"), 0);
			}
			set
			{
				SetValue("TendersID", value);
			}
		}


		/// <summary>
		/// Hero Title.
		/// </summary>
		[DatabaseField]
		public string TendersHeroTitle
		{
			get
			{
				return ValidationHelper.GetString(GetValue("TendersHeroTitle"), @"");
			}
			set
			{
				SetValue("TendersHeroTitle", value);
			}
		}


		/// <summary>
		/// Hero Image.
		/// </summary>
		[DatabaseField]
		public string TendersHeroImage
		{
			get
			{
				return ValidationHelper.GetString(GetValue("TendersHeroImage"), @"");
			}
			set
			{
				SetValue("TendersHeroImage", value);
			}
		}


		/// <summary>
		/// Gets an object that provides extended API for working with Tenders fields.
		/// </summary>
		[RegisterProperty]
		public TendersFields Fields
		{
			get
			{
				return mFields;
			}
		}


		/// <summary>
		/// Provides extended API for working with Tenders fields.
		/// </summary>
		[RegisterAllProperties]
		public partial class TendersFields : AbstractHierarchicalObject<TendersFields>
		{
			/// <summary>
			/// The content item of type Tenders that is a target of the extended API.
			/// </summary>
			private readonly Tenders mInstance;


			/// <summary>
			/// Initializes a new instance of the <see cref="TendersFields" /> class with the specified content item of type Tenders.
			/// </summary>
			/// <param name="instance">The content item of type Tenders that is a target of the extended API.</param>
			public TendersFields(Tenders instance)
			{
				mInstance = instance;
			}


			/// <summary>
			/// TendersID.
			/// </summary>
			public int ID
			{
				get
				{
					return mInstance.TendersID;
				}
				set
				{
					mInstance.TendersID = value;
				}
			}


			/// <summary>
			/// Hero Title.
			/// </summary>
			public string HeroTitle
			{
				get
				{
					return mInstance.TendersHeroTitle;
				}
				set
				{
					mInstance.TendersHeroTitle = value;
				}
			}


			/// <summary>
			/// Hero Image.
			/// </summary>
			public string HeroImage
			{
				get
				{
					return mInstance.TendersHeroImage;
				}
				set
				{
					mInstance.TendersHeroImage = value;
				}
			}
		}

		#endregion


		#region "Constructors"

		/// <summary>
		/// Initializes a new instance of the <see cref="Tenders" /> class.
		/// </summary>
		public Tenders() : base(CLASS_NAME)
		{
			mFields = new TendersFields(this);
		}

		#endregion
	}
}