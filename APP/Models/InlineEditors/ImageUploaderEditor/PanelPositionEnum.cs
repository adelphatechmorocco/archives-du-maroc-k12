﻿namespace ArchivesDuMaroc.Models.InlineEditors.ImageUploaderEditor
{
    public enum PanelPositionEnum
    {
        Top,
        Center,
        Bottom
    }
}