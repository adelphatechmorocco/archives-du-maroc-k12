﻿using System.Web;
using System.Web.Mvc;
using CMS.DocumentEngine;
using Kentico.Content.Web.Mvc;
using Kentico.PageBuilder.Web.Mvc;
using Kentico.Web.Mvc;

namespace ArchivesDuMaroc.Models.InlineEditors.ImageUploaderEditor
{
    public class ImageUploaderEditorViewModel : InlineEditorViewModel
    {
        //private const string MEDIA_LIBRARY_NAME = "ArchivesDuMaroc";

        public DocumentAttachment Image { get; set; }

        //public bool HasImage { get; set; }

        public bool UseAbsolutePosition { get; set; }

        public PanelPositionEnum MessagePosition { get; set; } = PanelPositionEnum.Center;

        //public ImageTypeEnum ImageType { get; set; } = ImageTypeEnum.Attachment;

        //public string GetDataUrl()
        //{
        //    var urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);

        //    if (ImageType == ImageTypeEnum.Attachment)
        //    {
        //        return urlHelper.Kentico().AuthenticateUrl(urlHelper.Action("Upload", "AttachmentImageUploader", new
        //        {
        //            pageId = HttpContext.Current.Kentico().PageBuilder().PageIdentifier
        //        }), false).ToString();
        //    }

        //    if (ImageType == ImageTypeEnum.MediaFile)
        //    {
        //        return urlHelper.Kentico().AuthenticateUrl(urlHelper.Action("Upload", "MediaFileImageUploader", new
        //        {
        //            libraryName = MEDIA_LIBRARY_NAME
        //        }), false).ToString();
        //    }

        //    return null;
        //}
    }
}