﻿using CMS.DocumentEngine.Types.AM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ArchivesDuMaroc.Models
{
    public class HomeViewModel
    {
        public string DocumentName { get; }

        public List<Slider> Slider { get; }
        public string YoutubeCode { get; }
        public string VideoCategory { get; }
        public string VideoTitle { get; }
        public string VideoDescription { get; }
        public PublicationItemViewModel lastPublicationItem { get; }

        // Maps the data from the Home page type's fields to the view model properties
        public HomeViewModel(Home homePage, List<Slider> slider, PublicationItemViewModel lastPublicationItem)
        {
            DocumentName = homePage.DocumentName;
            Slider = slider;
            VideoCategory = homePage.VideoCategory;
            VideoTitle = homePage.VideoTitle;
            VideoDescription = homePage.VideoDescription;
            YoutubeCode = homePage.LinkYoutube.Split(new string[] { "?v=" }, StringSplitOptions.None).Last().Split('&').First();
            this.lastPublicationItem = lastPublicationItem;
        }
    }
}