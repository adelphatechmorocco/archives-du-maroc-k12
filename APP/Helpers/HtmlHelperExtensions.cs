﻿using System;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;
using ArchivesDuMaroc.Config;
using ArchivesDuMaroc.Models.Menu;
using CMS.DocumentEngine;
using CMS.DocumentEngine.Types.AM;
using CMS.Helpers;
using CMS.SiteProvider;
using Kentico.Content.Web.Mvc;
using Kentico.Web.Mvc;

namespace ArchivesDuMaroc.Helpers
{
    public static class HtmlHelperExtensions
    {
        private static string _siteName = AppConfig.Sitename;
        private static string _cultureName = CultureInfo.CurrentCulture.Name;
        public static MvcHtmlString ValidatedEditorFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, string explanationText = "", bool disabled = false)
        {
            var label = html.LabelFor(expression).ToString();
            var additionalViewData = disabled ? new { htmlAttributes = new { disabled = "disabled" } } : null;
            var editor = html.EditorFor(expression, additionalViewData).ToString();
            var message = html.ValidationMessageFor(expression).ToString();
            var explanationTextHtml = "";

            if (!string.IsNullOrEmpty(explanationText))
            {
                explanationTextHtml = "<div class=\"explanation-text\">" + explanationText + "</div>";
            }

            var generatedHtml = string.Format(@"
                                        <div class=""form-group"">
                                            <div class=""form-group-label"">{0}</div>
                                            <div class=""form-group-input"">{1}
                                               {2}
                                            </div>
                                            <div class=""message message-error"">{3}</div>
                                        </div>", label, editor, explanationTextHtml, message);

            return MvcHtmlString.Create(generatedHtml);
        }

        public static MvcHtmlString MailTo(this HtmlHelper htmlHelper, string email)
        {
            if (string.IsNullOrEmpty(email))
            {
                return MvcHtmlString.Empty;
            }

            var link = string.Format("<a href=\"mailto:{0}\">{1}</a>", HTMLHelper.EncodeForHtmlAttribute(email), HTMLHelper.HTMLEncode(email));

            return MvcHtmlString.Create(link);
        }

        public static string AttachmentImage(this HtmlHelper htmlHelper, DocumentAttachment attachment, string title = "")
        {
            if (attachment == null)
            {
                return "";
            }

            return attachment.GetPath();
        }

        public static MvcHtmlString Image(this HtmlHelper htmlHelper, string path, string title = "", string cssClassName = "", SizeConstraint? constraint = null)
        {
            if (string.IsNullOrEmpty(path))
            {
                return MvcHtmlString.Empty;
            }

            var urlHelper = new UrlHelper(htmlHelper.ViewContext.RequestContext);
            var image = new TagBuilder("img");
            image.MergeAttribute("src", urlHelper.Kentico().ImageUrl(path, constraint.GetValueOrDefault(SizeConstraint.Empty)));
            image.AddCssClass(cssClassName);
            image.MergeAttribute("alt", title);
            image.MergeAttribute("title", title);

            return MvcHtmlString.Create(image.ToString(TagRenderMode.SelfClosing));
        }

        public static MvcHtmlString CultureLink(this HtmlHelper htmlHelper, string linkText, string cultureName)
        {
            var queryParams = htmlHelper.ViewContext.HttpContext.Request.Unvalidated.QueryString;
            var originalRouteValues = htmlHelper.ViewContext.RouteData.Values;

            if ((string)originalRouteValues["culture"] == cultureName)
            {
                return htmlHelper.HtmlLink(htmlHelper.ViewContext.HttpContext.Request.RawUrl, linkText);
            }

            var newRouteValues = new RouteValueDictionary(originalRouteValues);

            foreach (string key in queryParams)
            {
                if (string.IsNullOrEmpty(key)) continue;
                newRouteValues.Remove(key);
                newRouteValues.Add(key, queryParams[key]);
            }

            newRouteValues["culture"] = cultureName;

            return htmlHelper.RouteLink(linkText, newRouteValues);
        }

        public static MvcHtmlString HtmlLink(this HtmlHelper htmlHelper, string url, string linkText, object htmlAttributes = null)
        {
            var link = new TagBuilder("a") { InnerHtml = linkText };
            link.MergeAttributes(new RouteValueDictionary(htmlAttributes));
            link.MergeAttribute("href", url);

            return MvcHtmlString.Create(link.ToString());
        }

        public static IHtmlString GetUiString(this HtmlHelper htmlHelper, string key)
        {
            return htmlHelper.Raw(ResHelper.GetString(key));
        }

        public static MenuItemViewModel GetMenuItem(this HtmlHelper htmlHelper, MenuItem menuItem)
        {
            MenuItem menu = MenuItemProvider.GetMenuItem(menuItem.NodeGUID, CultureInfo.CurrentCulture.Name, _siteName);
            var pages = DocumentHelper.GetDocuments().Culture(CultureInfo.CurrentCulture.Name).OnSite(_siteName)
                .WhereEquals("NodeGUID", menu.MenuItemPage)
                .Columns("NodeGUID", "DocumentCulture", "DocumentName", "NodeAlias", "NodeAliasPath").FirstOrDefault();

            var model = new MenuItemViewModel
            {
                MenuItemText = menu.MenuItemText,
                MenuItemRelativeUrl = pages?.RelativeURL,
                Children = menu.Children
            };
            return model;
        }

        public static string GetTreeLinks(this HtmlHelper htmlHelper, Guid guid)
        {
            var menuItem = DocumentHelper.GetDocuments<MenuItem>().Culture(CultureInfo.CurrentCulture.Name).OnSite(_siteName)
                .WhereEquals("MenuItemPage", guid)
                .Columns("NodeGUID", "DocumentCulture", "DocumentName", "MenuItemText", "NodeParentID", "MenuItemPage").FirstOrDefault();
            if (menuItem == null || menuItem.Parent == null)
                return "";
            return GetParentItem(menuItem.Parent);
        }
        public static string GetParentItem(TreeNode parent)
        {
            if (parent.NodeLevel > 1)
            {
                var menuItem = (MenuItem)parent;
                var page = DocumentHelper.GetDocuments().Culture(CultureInfo.CurrentCulture.Name).OnSite(_siteName)
                  .WhereEquals("NodeGUID", menuItem.MenuItemPage)
                  .Columns("NodeGUID", "DocumentCulture", "DocumentName", "NodeAlias", "NodeAliasPath").FirstOrDefault();
                var li = GetParentItem(parent.Parent);
                li += "<li><a href='" + (page != null && !string.IsNullOrEmpty(page.AbsoluteURL) ? page.AbsoluteURL : "#") + "'>" + menuItem.MenuItemText + "</a></li>";
                return li;
            }
            return "";
        }
    }
}