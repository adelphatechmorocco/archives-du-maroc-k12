﻿using Kentico.Content.Web.Mvc;
using ArchivesDuMaroc.Models;
using Kentico.Web.Mvc;
using System.Web.Mvc;

namespace ArchivesDuMaroc.Helpers.Extensions
{
    public static class UrlExtensions
    {
        public static string KenticoImageUrl(this UrlHelper helper, string path, IImageSizeConstraint size = null)
        {
            return helper.Kentico().ImageUrl(path, size?.GetSizeConstraint() ?? SizeConstraint.Empty);
        }
    }
}